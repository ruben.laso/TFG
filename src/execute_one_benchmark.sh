#!/bin/bash

# $1 must be the name of the executable -- without the extension!!! --

sizes_of_matrix=(4000)
number_of_threads=(1 2 4 8 16 32 68 136 204 272)

echo "Size of matrix;Number of threads;Time (seconds)" > $1.csv
for size in ${sizes_of_matrix[@]}
do
    for threads in  ${number_of_threads[@]}
    do
        echo -n "Benchmark $1 with size $size and $threads threads doing -> "
        ./$1.out -s $size -t $threads >> $1.csv
        echo "done"
    done
    echo "Short benchmarks done"
done

sizes_of_matrix=(8000 16000)
number_of_threads=(68 136 204 272)
for size in ${sizes_of_matrix[@]}
do
    for threads in  ${number_of_threads[@]}
    do
        echo -n "Benchmark $1 with size $size and $threads threads doing -> "
        ./$1.out -s $size -t $threads >> $1.csv
        echo "done"
    done
done

sizes_of_matrix=(32000)
number_of_threads=(68 272)
for size in ${sizes_of_matrix[@]}
do
    for threads in  ${number_of_threads[@]}
    do
        echo -n "Benchmark $1 with size $size and $threads threads doing -> "
        ./$1.out -s $size -t $threads >> $1.csv
        echo "done"
    done
done

echo "All benchmarks done"
