#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <hbwmalloc.h>

typedef struct {
    double * data;
    int number_of_rows;
    int number_of_columns;
} st_matrix;

typedef st_matrix * matrix;

bool initialize_matrix (const int number_of_rows, const int number_of_columns, matrix * _matrix) {
    (*_matrix) = (matrix) hbw_malloc (sizeof(st_matrix));

    if ((*_matrix) == NULL) {
        return false;
    }

    (*_matrix)->data = (double *) hbw_malloc (number_of_rows * number_of_columns * sizeof(double));
    if ((*_matrix)->data == NULL) {
        return false;
    }

    (*_matrix)->number_of_rows = number_of_rows;
    (*_matrix)->number_of_columns = number_of_columns;

    return true;
}

// Assign value 0 (cero) to all the elements of the matrix
// returns true if succes, false otherway
bool initialize_matrix_to_cero (const matrix _matrix) {
    int i, j;

    for (i = 0; i < (*_matrix).number_of_rows; i++) {
        for (j = 0; j < (*_matrix).number_of_columns; j++) {
            (*_matrix).data [i * (*_matrix).number_of_columns + j] = 0;
        }
    }

    return true;
}


// Assign a value given by the indexes of the element and the size of the matrix by the form i*j/size^2
// returns true if succes, false otherway
bool initialize_matrix_by_indexes (const matrix _matrix) {
    int i, j;
    double size = (*_matrix).number_of_rows * (*_matrix).number_of_columns;

    for (i = 0; i < (*_matrix).number_of_rows; i++) {
        for (j = 0; j < (*_matrix).number_of_columns; j++) {
            (*_matrix).data [i * (*_matrix).number_of_rows + j] = (i * j) / size;
        }
    }

    return true;
}

// Print the content of the matrix in the file called filename
// return true id succes, false otherway
bool print_matrix_file (const char * filename, const matrix _matrix) {
    int i, j;
    FILE * fp;

    fp = fopen(filename, "w");

    if (fp == NULL) {
        return false;
    }

    for (i = 0; i < (*_matrix).number_of_rows; i++) {
        for (j = 0; j < (*_matrix).number_of_columns; j++) {
            fprintf(fp, "%f ", (*_matrix).data [i * (*_matrix).number_of_rows + j] );
        }
        fprintf(fp, "\n");
    }

    fclose(fp);

    return true;
}

// Frees the memory of _matrix
void destroy_matrix (matrix * _matrix) {
    hbw_free((*_matrix)->data);
    hbw_free(*_matrix);
}
