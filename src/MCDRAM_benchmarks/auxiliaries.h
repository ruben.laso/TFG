#ifndef AUXILIARIES
#define AUXILIARIES

#include <stdbool.h>

typedef struct {
    double * data;
    int number_of_rows;
    int number_of_columns;
} st_matrix;

typedef st_matrix * matrix;

// Reserves memory for the matrix structure
bool initialize_matrix (const int number_of_rows, const int number_of_columns, matrix * _matrix);

// Assign value 0 (cero) to all the elements of the matrix
// returns true if succes, false otherway
bool initialize_matrix_to_cero (const matrix _matrix);

// Assign a value given by the indexes of the element and the size of the matrix by the form i*j/size^2
// returns true if succes, false otherway
bool initialize_matrix_by_indexes (const matrix _matrix);

// Print the content of the matrix in the file called filename
// return true if succes, false otherway
bool print_matrix_file (const char * filename, const matrix _matrix);

// Frees the memory of _matrix
void destroy_matrix (matrix * _matrix);

#endif
