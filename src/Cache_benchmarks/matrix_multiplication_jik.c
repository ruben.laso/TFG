#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <getopt.h>
#include "auxiliaries.h"

static int size_of_matrix = 1000;
static int number_of_threads = 1;
static int real_number_of_threads = 0;
static bool print_matrices = false;
static bool debug_mode = false;

static const char * f_matrix_result = "matrix_result.txt";
static const char * f_matrix_a = "matrix_a.txt";
static const char * f_matrix_b = "matrix_b.txt";

static const struct option long_opts [] = {
    {"size",        required_argument,  NULL,   's'},
    {"threads",     required_argument,  NULL,   't'},
    {"print",       no_argument,        NULL,   'p'},
    {"verbose",     no_argument,        NULL,   'v'},
};

static const char short_opts [] = "s:t:pv";

static void process_arguments (int argc, char const * argv []);


int main(int argc, char const *argv[]) {
    matrix matrix_result, matrix_a, matrix_b;
    double * matrix_result_pointer, * matrix_a_pointer, * matrix_b_pointer;
    int i_index_iterations;
    int i, j, k;

    struct timeval start, end;
    float time_in_seconds;

    process_arguments(argc, argv);

    if (debug_mode) {
        printf("Size of matrix = %d\n", size_of_matrix);
        printf("Number of threads: %d\n", number_of_threads);
        printf("Print matrices: %d\n", print_matrices);
    }

    if (!initialize_matrix(size_of_matrix, size_of_matrix, &matrix_result)) {
        fprintf(stderr, "Failed to initialize the matrix. Exiting the program\n");
        exit(1);
    } else {
        initialize_matrix_to_cero(matrix_result);
    }

    if (!initialize_matrix(size_of_matrix, size_of_matrix, &matrix_a)) {
        destroy_matrix(&matrix_result);
        fprintf(stderr, "Failed to initialize the matrix. Exiting the program\n");
        exit(1);
    } else {
        initialize_matrix_by_indexes(matrix_a);
    }

    if (!initialize_matrix(size_of_matrix, size_of_matrix, &matrix_b)) {
        destroy_matrix(&matrix_result);
        destroy_matrix(&matrix_a);
        fprintf(stderr, "Failed to initialize the matrix. Exiting the program\n");
        exit(1);
    } else {
        initialize_matrix_by_indexes(matrix_b);
    }

    if (debug_mode) {
        printf("Matrices initialized\n");
    }

    matrix_result_pointer = matrix_result->data;
    matrix_a_pointer = matrix_a->data;
    matrix_b_pointer = matrix_b->data;

    i_index_iterations = size_of_matrix * size_of_matrix;

    gettimeofday(&start, NULL);
    #pragma omp parallel for num_threads(number_of_threads) private(i,j,k)
    for (j = 0; j < size_of_matrix; j++) {
        for (i = 0; i < i_index_iterations; i+=size_of_matrix) {
            for (k = 0; k < size_of_matrix; k++) {
                matrix_result_pointer [i + j] += matrix_a_pointer [i + k] * matrix_b_pointer [k*size_of_matrix + j];
            }
        }
    }
    gettimeofday(&end, NULL);

    time_in_seconds = (end.tv_sec-start.tv_sec+(end.tv_usec-start.tv_usec)/1.e6);

    #pragma omp parallel num_threads(number_of_threads)
    {
        #pragma omp single
        real_number_of_threads = omp_get_num_threads();
    }

    if (debug_mode) {
        printf("Real number of threads executed = %d\n", real_number_of_threads);
        printf("Multiplication done. Printing time results and destroying matrices\n");
    }

    printf("%d;%d;%f\n", size_of_matrix, number_of_threads, time_in_seconds);

    if (print_matrices) {
        print_matrix_file(f_matrix_a, matrix_a);
        print_matrix_file(f_matrix_b, matrix_b);
        print_matrix_file(f_matrix_result, matrix_result);
    }

    destroy_matrix(&matrix_result);
    destroy_matrix(&matrix_a);
    destroy_matrix(&matrix_b);

    return 0;
}

static void process_arguments (int argc, char const * argv []) {
    int rv;
    for (
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL);
            rv != -1;
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL)
        ) {
        switch (rv) {
            case 's':
                size_of_matrix = atoi(optarg);
                break;
            case 't':
                number_of_threads = atoi(optarg);
                break;
            case 'p':
                print_matrices = true;
                break;
            case 'v':
                debug_mode = true;
                break;
            default:
                break;
        }
    }
}
