#!/bin/bash

list_of_benchmarks=(matrix_multiplication_ijk matrix_multiplication_ikj matrix_multiplication_kij matrix_multiplication_kji matrix_multiplication_jik matrix_multiplication_jki)
sizes_of_matrix=(4000)
number_of_threads=(1 2 4 8 16 32 68 136 204 272)

for benchmark in ${list_of_benchmarks[@]}
do
    echo "Size of matrix;Number of threads;Time (seconds)" > $benchmark.csv
    for size in ${sizes_of_matrix[@]}
    do
        for threads in  ${number_of_threads[@]}
        do
            echo -n "Benchmark $benchmark with size $size and $threads threads doing -> "
            ./$benchmark.out -s $size -t $threads >> $benchmark.csv
            echo "done"
        done
    done
done

sizes_of_matrix=(8000 16000)
number_of_threads=(68 136 204 272)
for benchmark in ${list_of_benchmarks[@]}
do
    for size in ${sizes_of_matrix[@]}
    do
        for threads in  ${number_of_threads[@]}
        do
            echo -n "Benchmark $benchmark with size $size and $threads threads doing -> "
            ./$benchmark.out -s $size -t $threads >> $benchmark.csv
            echo "done"
        done
    done
done

sizes_of_matrix=(32000)
number_of_threads=(68 272)
for benchmark in ${list_of_benchmarks[@]}
do
    for size in ${sizes_of_matrix[@]}
    do
        for threads in  ${number_of_threads[@]}
        do
            echo -n "Benchmark $benchmark with size $size and $threads threads doing -> "
            ./$benchmark.out -s $size -t $threads >> $benchmark.csv
            echo "done"
        done
    done
done

echo "All benchmarks done"
