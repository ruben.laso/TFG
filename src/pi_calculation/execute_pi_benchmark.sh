#!/bin/bash

list_of_benchmarks=(pi)
_1e6=1000000
_1e7=10000000
_1e8=100000000
number_of_iterations=($_1e6 $_1e7 $_1e8)
number_of_threads=(1 2 4 8 16 32 68 136 204 272)

for benchmark in ${list_of_benchmarks[@]}
do
    echo "Number of iteration; Number of threads; Time (seconds)" > $benchmark.csv
    for size in ${number_of_iterations[@]}
    do
        for threads in  ${number_of_threads[@]}
        do
            echo -n "Benchmark $benchmark with $size iteration and $threads threads doing -> "
            ./$benchmark.out $size $threads >> $benchmark.csv
            echo "done"
        done
    done
done

_1e9=1000000000
_1e10=10000000000
number_of_iterations=($_1e9 $_1e10)
number_of_threads=(68 136 204 272)

for benchmark in ${list_of_benchmarks[@]}
do
    for size in ${number_of_iterations[@]}
    do
        for threads in  ${number_of_threads[@]}
        do
            echo -n "Benchmark $benchmark with $size iteration and $threads threads doing -> "
            ./$benchmark.out $size $threads >> $benchmark.csv
            echo "done"
        done
    done
done

echo "All benchmarks done"
