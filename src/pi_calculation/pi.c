#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <omp.h>
#include <sched.h>

long int max_k = 1000000;
const int base = 10;

int main(int argc, char const *argv[]) {
    struct timeval inicio, final;
    double pi = 0;
    long int k, _8k;
    int number_of_threads = 0;

    if (argc > 1) {
        max_k = strtol(argv[1], NULL, base);
    }

    if (argc > 2) {
        number_of_threads = atoi(argv[2]);
    }

    gettimeofday(&inicio, NULL);
    #pragma omp parallel for reduction(+: pi) private (k, _8k) num_threads(number_of_threads)
    for (k = 0; k < max_k; k++) {
        _8k = 8 * k;
        pi += 1.0/pow(16,k) * (4.0/(_8k + 1) - 2.0/(_8k + 4) - 1.0/(_8k + 5) - 1.0/(_8k + 6));
    }
    gettimeofday(&final, NULL);

    #pragma omp parallel num_threads(number_of_threads)
    {
        #pragma omp single
        {
            number_of_threads = omp_get_num_threads();
        }
        if (argc > 3) {
            int thread_num = omp_get_thread_num();
            int cpu_num = sched_getcpu();
            printf("Thread %3d is running on CPU %3d\n", thread_num, cpu_num);
        }
    }

    if (argc > 3) {
        printf("Pi = %10f\n", pi);
    }

    printf("%ld;%d;%f\n", max_k, number_of_threads, (final.tv_sec-inicio.tv_sec+(final.tv_usec-inicio.tv_usec)/1.e6));

    return 0;
}
