#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <getopt.h>
#include <hbwmalloc.h>

static long size_of_array = 10000000;
static long repetitions = 0;
static int step = 1;

static const struct option long_opts [] = {
    {"size",        required_argument,  NULL,   's'},
    {"repetitions", required_argument,  NULL,   'r'},
    {"step",        required_argument,  NULL,   'S'},
};

static const char short_opts [] = "s:r:S:";

static void process_arguments (int argc, char const * argv []);

int main(int argc, char const *argv[]) {
    double * array;
    int i, j, k;

    struct timeval start, end;
    float time_in_seconds;

    process_arguments(argc, argv);

    array = (double *) hbw_malloc(size_of_array * sizeof(double));
    if (array == NULL) {
        return EXIT_FAILURE;
    }

    gettimeofday(&start, NULL);
    for (i = 0; i < repetitions; i++) {
        for (j = 0, k = 0; k < size_of_array; k++, j = (j + step) % size_of_array) {
            array[j] = array[j] + 0.001;
        }
    }
    gettimeofday(&end, NULL);

    time_in_seconds = (end.tv_sec-start.tv_sec+(end.tv_usec-start.tv_usec)/1.e6);

    printf("%ld;%ld;%d;%f\n", size_of_array, repetitions, step, time_in_seconds);

    hbw_free(array);

    return EXIT_SUCCESS;
}

static void process_arguments (int argc, char const * argv []) {
    int rv;
    for (
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL);
            rv != -1;
            rv = getopt_long(argc, (char * const *)argv, short_opts, long_opts, NULL)
        ) {
        switch (rv) {
            case 's':
                size_of_array = atol(optarg);
                break;
            case 'r':
                repetitions = atol(optarg);
                break;
            case 'S':
                step = atoi(optarg);
                break;
            default:
                break;
        }
    }
}
