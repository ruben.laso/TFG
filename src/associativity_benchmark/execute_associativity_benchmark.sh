#!/bin/bash

list_of_benchmarks=(associativity)
_1e5=100000
_1e6=1000000
_1e7=10000000
_1e8=100000000
number_of_iterations=($_1e6)
number_of_repetitions=(1000)
steps=(1 2 4 8 16 32 64 128 256 512 1024 2048)

for benchmark in ${list_of_benchmarks[@]}
do
    echo "Size of array;Repetitions;Step;Time (seconds)" > $benchmark.csv
    for size in ${number_of_iterations[@]}
    do
      	for repetitions in  ${number_of_repetitions[@]}
        do
            for step in ${steps[@]}
            do
              	echo -n "Benchmark $benchmark with $size iteration,  $repetitions repetitions and step $step doing -> "
                ./$benchmark.out -s $size -r $repetitions -S $step >> $benchmark.csv
                echo "done"
            done
        done
    done
done

echo "All benchmarks done"
