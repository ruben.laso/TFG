Repositorio del Trabajo de Fin de Grado de Ruben Laso Rguez: Estudio del comportamiento de un Intel Xeon Phi ante diferentes configuraciones de memoria y clustering.

Realizado en la Universidad de Santiago de Compostela. 2016.

Tutorizado por: D. José Carlos Cabaleiro Domínguez, Profesor do Departamento de Electrónica e Computación da Universidade de Santiago de Compostela, e D. Francisco Fernández Rivera, Profesor do Departamento de Electrónica e Computación da Universidade de Santiago de Compostela.