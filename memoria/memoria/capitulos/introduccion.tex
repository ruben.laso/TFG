\chapter{Introducción} \label{sec:introduccion}
Antes de proceder a explicar todo el trabajo realizado en este proyecto, se ha de realizar una breve introducción para poner en contexto al lector y explicar ciertos elementos que faciliten la lectura de esta memoria, así como ayudarle a comprender qué objetivos y motivaciones hay detrás de este proyecto y por qué se ha realizado.

\section{Historia de la computación paralela}
Los orígenes de la computación paralela se remontan hasta finales de los años 50, donde se empezarían a perfilar algunas de sus características, aunque no sería hasta los 60-70 cuando estas se empezarán a poner en marcha con la aparición de los computadores con memoria compartida. En estos computadores se situaban varios procesadores trabajando simultáneamente, compartiendo los datos en memoria~\cite{parallel-computing-intel-history}. 
Esto presentaba varios retos para la época, principalmente en lo que a mantener la coherencia de datos se refería, aunque en ese momento de la historia de la computación, los datos y su acceso no eran el cuello de botella, sino que lo eran las operaciones aritméticas.

En estos años, se desarrolló el ILLIAC-IV (\textit{Illinois Automatic Computer}), uno de los primeros supercomputadores, el cual se diseñó en un principio para utilizar 256 procesadores funcionando en cuatro conjuntos de 64 PU (\textit{Processing Units}) que operaban con registros de 64 bits cada uno, intentando llegar a la meta de $10^9$ FLOPS (\textit{Floating point Operations Per Second}) \cite{Illiac-IV}, aunque realmente, y con el paso de los años, se quedaron cerca de 200 MFLOPS. Para poner en contraste estas cifras, podemos compararlo con un iPhone 6, el cual ya multiplica por 58 la capacidad computacional objetivo del ILLIAC-IV \cite{iPhone-FLOPS}.

Contemporáneamente al ILLIAC-IV, en 1976, se instaló el Cray 1, uno de los supercomputadores más famosos de la historia. Este computador funcionaba a 80 MHz, un récord para la época y era capaz de alcanzar 80 MFLOPS, que utilizando las opciones de vectorización oportunas, ocho registros vectoriales con capacidad para 64 números en punto flotante de doble precisión, llegarían hasta los 250~MFLOPS~\cite{Cray-1}.

Ya en los años 80, surgió el \textit{Caltech Concurrent Computation Project}, el cual pretendía utilizar 64 procesadores Intel 8086/8087 de forma concurrente para alcanzar un rendimiento de varios GFLOPS, demostrando así que se podía conseguir un alto rendimiento utilizando ``simples'' procesadores de propósito general.
Siguiendo esta línea, en el año 1997, el ASCI Red Supercomputer superó la barrera de 1 TFLOPS utilizando MPPs (\textit{Massively Parallel Processors}).

También en los años 80, aparecieron los clústers de computación. Estos clústers agrupan un conjunto grande de computadores estándar conectados mediante una red local de gran rendimiento. Esta solución es la adoptada actualmente por ser la más barata en términos de montaje y mantenimiento, la más flexible con los servicios utilizados, así como la más escalable. 

En origen, los procesadores utilizados en este tipo de supercomputadores eran procesadores con un único núcleo, pero debido al agotamiento del escalamiento de los procesadores \emph{unicore} en la Ley de Moore, se ha avanzado primero hacia los procesadores \emph{multicore} y ya en los últimos años a los procesadores \emph{manycore} (para más detalle, ver sección~\ref{sec:procesador-manycore}, pág.~\pageref{sec:procesador-manycore}).

Hoy en día tenemos procesadores que cuentan con varios núcleos en todos nuestros dispositivos, aunque es especialmente en los servidores de supercomputación donde más se nota esta evolución, llegando a la docena de \textit{cores} en la mayoría de CPUs, a varias decenas en procesadores como el Intel Xeon Phi que se estudiará en este \tfg{} o a los cientos y miles en las GPUs instaladas en clústers actuales.


\section{Intel Xeon Phi \textit{Knights Landing}}
Como resultado del escalamiento hacia un mayor número de \textit{cores} en lugar de núcleos más potentes, en los últimos años la industria ha apostado por el uso de procesadores \textit{manycore}, bien en forma de aceleradores y ``coprocesadores'' como las tarjetas de NVIDIA y su tecnología CUDA, o bien como procesadores \textit{per se} como el Intel Xeon Phi KNL.

Este procesador se diferencia del resto de productos de Intel en el gran número de núcleos que contiene, así como los componentes concretos de los mismos. En este sentido en la generación \textit{Knights Landing} del Intel Xeon Phi, cada procesador cuenta con 64-72 \textit{cores} (divididos en 32-36 celdas o \textit{tiles}) cada uno con dos unidades de procesamiento vectorial (VPU).
Esto permite, junto a la tecnología \textit{hyperthreading}, ejecutar de forma paralela cientos de \textit{threads} en un único procesador, efectuando así una gran cantidad de operaciones de forma simultánea y teniendo un pico teórico de rendimiento de unos 3 TFLOPS.

Por otro lado, el Intel Xeon Phi cuenta con dos elementos configurables especialmente interesantes desde el punto de vista de la computación de altas prestaciones. Estos son la memoria de alto rendimiento MCDRAM y los modos de \textit{clustering} de las celdas que forman el procesador.

En cuanto al primer elemento, el Intel Xeon Phi monta una memoria de 16 GB de capacidad con un alto ancho de banda (hasta 400 GB/s) conocida como MCDRAM (\textit{Multi-Channel Dynamic Random Access Memory}). Esta memoria puede configurarse para funcionar de tres modos diferentes. En el modo caché actuará como una memoria caché de último nivel tradicional, en el modo \textit{flat} se utilizará como otra memoria principal y en el modo híbrido parte de la memoria actuará en modo caché y otra parte en modo \textit{flat}.

Sobre los modos de \textit{clustering}, se pueden establecer hasta 5 configuraciones diferentes. Estos modos determinan cómo se comunican las celdas del procesador entre ellas y con la memoria principal y cómo ve el sistema operativo al mismo, ya sea como un único procesador o como varios. En modo \textit{all-to-all} todas las celdas podrán comunicarse entre sí como una única unidad. 
En el modo \textit{hemisphere}/\textit{quadrant} se divide el chip en dos/cuatro regiones para localizar los accesos a memoria, mientras el sistema operativo sigue viendo el procesador como un único elemento.
Por último en modo SNC-2/SNC-4 se realiza una división similar a la del modo \textit{quadrant} con la salvedad de que en este modo el sistema operativo es consciente de la división, obteniendo así dos/cuatro nodos NUMA diferentes.

\section{Objetivos del proyecto}
En este \tfg{} se pretende realizar un estudio del comportamiento del Intel Xeon Phi ante las diferentes configuraciones de las que dispone, para ayudar a los futuros usuarios de esta máquina a comprenderla y configurarla con el fin de obtener el mejor rendimiento posible en función de sus necesidades.

Así pues, se medirá el rendimiento y se estudiará cómo afecta la configuración aplicada a la máquina en lo que a los accesos a memoria se refiere (el punto más crítico en los programas actuales). En concreto nos centraremos en dos elementos de la configuración que afectan a este acceso a datos: la configuración de la memoria MCDRAM (caché, \textit{flat} e híbrida) y los modos de \textit{clustering} (\textit{all-to-all}, \textit{quadrant} y SNC-4). Se espera que la combinación de estos modos provoque comportamientos sensiblemente diferentes, lo cuales serán recogidos y explicados en este \tfg{}.

Para llevar a cabo este proyecto, se diseñarán y utilizarán diversos benchmarks, los cuales serán implementados usando el lenguaje de programación C y la API de programación paralela OpenMP. Además, se hará uso de las funciones provistas por las librerías \texttt{memkind} y \texttt{hbwmalloc} para controlar la memoria de alto rendimiento que incorpora el Intel Xeon Phi.

Es necesario remarcar que la meta fundamental de este trabajo de investigación es establecer las causas del mejor o peor rendimiento del procesador para que los futuros usuarios del Xeon Phi \textit{Knights Landing} sepan adecuar su software y configurar el sistema para obtener el mayor rendimiento posible.

\section{Motivación del proyecto}
Este proyecto puede situarse en el marco de la computación de altas prestaciones u orientada a grandes centros de procesamiento de datos, donde se utilizan procesadores específicos para este contexto. Estos procesadores suelen contar con una serie de características principales por las que se justifica su gran coste: la alta frecuencia de reloj con la que trabajan, el tamaño de las memorias cachés que incorporan, las unidades de cálculo específicas que integran, el mayor número de núcleos, memorias de alto rendimiento, etc.
En relación a estas características, el Intel Xeon Phi, además de ser considerado un procesador \textit{manycore} debido a que tiene decenas de núcleos, incorpora una memoria de alto rendimiento conocida como memoria MCDRAM. A mayores, el Xeon Phi cuenta con dos elementos que pueden ser configurados, por un lado se puede cambiar la afinidad de los \textit{cores}, cómo estos acceden a la memoria y cómo ve el sistema operativo al procesador, y por otro, cómo se comporta la memoria MCDRAM.

Poder obtener un modelo del comportamiento del Intel Xeon Phi y saber cómo afectan las diferentes configuraciones aplicables al procesador es de gran importancia para sacar el mayor provecho posible del procesador. Tanto si somos usuarios expertos en paralelismo y en la optimización de código como si somos usuarios con un carácter más general, siempre buscamos que los tiempos de ejecución sean los más pequeños posibles para ahorrar dinero, pues al fin y al cabo el tiempo es dinero y tener este tipo de componentes en funcionamiento es realmente costoso.

Así pues, la principal motivación de este proyecto es el intentar ayudar a los futuros usuarios del Intel Xeon Phi a seleccionar la configuración que más le convenga, bien por tiempo de ejecución, por tiempo de desarrollo, etc. mediante una caracterización del comportamiento del Intel Xeon Phi.
También existen otras líneas secundarias que pueden resultar interesantes en este proyecto como puede ser el obtener la cuantificación del rendimiento del procesador o de la memoria MCDRAM que incorpora el Intel Xeon Phi KNL respecto a procesadores y memorias convencionales o saber qué complicaciones tiene el desarrollar aplicaciones para este procesador.

\section{Formato numérico utilizado}
Se ha de mencionar que todos los datos incluidos en este documento están mostrados conforme al estándar internacional donde la coma (,) separa las unidades de millar y el punto (.) es el separador decimal.