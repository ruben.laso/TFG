\subsection{Identificación de activos, amenazas y riesgos} \label{sec:identificacion-riesgos}
En la identificación de riesgos del proyecto se utilizarán diversas técnicas y metodologías con el fin de llegar a un análisis preciso. Estas metodologías son:

\begin{itemize}
	\item Metodología tradicional: utilizando la experiencia del equipo y analizando las características propias del proyecto, se identifican en primer lugar los activos, posteriormente, las amenazas que pueden afectar a los activos y, por último, los riesgos derivados de las amenazas que afectarían a los activos.
	
	\item Diagramas de Ishikawa, o de causa-efecto: estos diagramas identifican las causas que desencadenarían un riesgo concreto durante la realización del proyecto. Se realizarán diagramas para aquellos riesgos que conlleven una mayor exposición debido a su alta probabilidad e impacto.
\end{itemize}

\subsubsection{Activos}
Se consideran activos aquellos elementos de valor intelectual o capital para el proyecto y que son susceptibles de ser dañados por una amenaza.

Como es lógico, estos activos deben ser preservados para que el proyecto y sus integrantes puedan seguir beneficiándose de ellos, y que el desarrollo del mismo sea favorable con el fin de conseguir los objetivos marcados.

En la tabla \ref{tab:activos} se muestra la lista de activos obtenidos durante el proceso de análisis.

\input{capitulos/gestion-proyecto/riesgos/activos}

\subsubsection{Amenazas}
Las amenazas son aquellos hechos que de forma intencionada, o no, pueden dañar alguno de los activos.

En la tabla \ref{tab:amenazas} se muestran la lista de amenazas identificadas para este proyecto.

\input{capitulos/gestion-proyecto/riesgos/amenazas}

\subsubsection{Riesgos}
Un riesgo es la posibilidad de que una amenaza explote una vulnerabilidad de un activo, produciendo un efecto negativo en un proyecto. De darse un riesgo, la gestión de riesgos determinará cómo se debe actuar para reducir el impacto negativo y, de ser posible, aprovechar un impacto positivo.

Antes de proceder a comentar los riesgos que se han identificado en este proyecto, es pertinente comentar la matriz de probabilidad/impacto que se ha utilizado para valorar la exposición de los riesgos.

En la matriz de probabilidad/impacto se utilizan los siguientes términos:
\begin{itemize}
	\item Probabilidad: es la expectativa de que un riesgo suceda durante el transcurso del proyecto.
	
	\item Impacto: representa el efecto (en tiempo, esfuerzo o dinero) que supondría en el proyecto la aparición de un riesgo en caso de no ser gestionado adecuadamente.
	
	\item Exposición: calculado como el producto $p \times i$ de la probabilidad $p$ y el impacto $i$, representa el factor en que el proyecto puede verse afectado por la aparición de un riesgo. Este parámetro será utilizado a fin de ordenar y seleccionar los riesgos para priorizar las acciones de aquellos que sean más graves.
\end{itemize}

En nuestro caso, los índices de probabilidad e impacto tomarán los valores siguientes:
\begin{itemize}
	\item Probabilidad:
		\subitem Baja:  $\left[ 0\% - 10\% \right) \rightarrow 0.05$
		\subitem Media: $\left[ 10\% - 40\% \right) \rightarrow 0.30$
		\subitem Alta:  $\left[ 40\% - 100\% \right] \rightarrow 0.70$
		
		
	\item Impacto:
		\subitem Bajo:  $\left[ 0\% - 20\% \right) \rightarrow 0.10$
		\subitem Medio: $\left[ 20\% - 50\% \right) \rightarrow 0.35$
		\subitem Alto:  $\left[ 50\% - 100\% \right] \rightarrow 0.75$
\end{itemize}

En esta matriz, la exposición de los riesgos puede clasificarse tal que así:
\begin{itemize}
	\item Baja: $\left[ 0 - 0.0150\right)$
	\item Media-Baja: $\left[ 0.0150 - 0.0350\right)$
	\item Media: $\left[ 0.0350 - 0.1500\right)$
	\item Media-Alta: $\left[ 0.1500 - 0.2500\right)$
	\item Alta: $\left[ 0.2500 - \infty \right)$
\end{itemize}


Así pues, con estos valores se llega a la tabla \ref{tab:matrizprobimp}, donde se puede ver la Matriz de Probabilidad e Impacto establecida para este proyecto.

\input{capitulos/gestion-proyecto/riesgos/matriz-probabilidad-impacto}

Como es lógico, cuanto mayor sea la exposición de un riesgo, más atención hay que prestar a su tratamiento y mejor tiene que ser el plan de acción correspondiente. De esta forma, un riesgo con una exposición baja podría ser aceptado por el proyecto sin demasiados problemas, mientras que aquellos riesgos con exposición alta deberían ser tratados con el máximo cuidado a la par que con la mayor celeridad para que su impacto en el proyecto sea el menor posible.

Para cada uno de los riesgos, debe establecerse una medida de tratamiento. El PMBOK aconseja varios tipos de estrategias, entre las cuales están:

\begin{itemize}
	\item Mitigación: una vez aparecido el riesgo, desde el equipo del proyecto se realizarán una serie de acciones para limitar el impacto negativo que puedan tener.
	
	\item Transferencia: la responsabilidad de actuación ante un riesgo se traspasa a terceras entidades, como podrían ser compañías de seguros, compañías de seguridad informática, etc.
	
	\item Aceptación: en caso de que un riesgo aparezca, simplemente se aceptan sus efectos y se intenta proseguir con el proyecto de forma normal adaptándolo convenientemente. Esta medida sólo es recomendable para aquellos riesgos cuya probabilidad e impacto sean tan leves que no provocarían en ningún caso un riesgo para la salud del proyecto, o para riesgos que serían inevitables y que finalizarían el proyecto inmediatamente sin poder tomar ninguna otra medida.
\end{itemize}

A continuación, se presentan los riesgos identificados para este proyecto clasificados en cuanto a su probabilidad, impacto y exposición, y ordenados por esta última cualidad. Además, se incluyen las medidas establecidas para cada uno de los riesgos y los responsables de llevarlas a cabo.

% riesgos de alta exposicion
\paragraph{Riesgos de exposición \textit{alta}} Estos riesgos, de darse, requieren de una acción inmediata y eficaz para evitar los daños en el proyecto, pues podrían llegar a suponer retrasos importantes, o incluso la cancelación del mismo. Además es muy probable que se den, por lo que el equipo del proyecto debe estar preparado para abordarlos.

\input{capitulos/gestion-proyecto/riesgos/planificacion-incorrecta}

\input{capitulos/gestion-proyecto/riesgos/duracion-benchmarks}

\input{capitulos/gestion-proyecto/riesgos/falta-formacion}

\paragraph{Riesgos de exposición \textit{media-alta}} Al igual que los riesgos con exposición alta, su aparición podría perjudicar muy gravemente al proyecto, aunque o bien son menos probables, o su impacto es menor en relación a los anteriores.

\input{capitulos/gestion-proyecto/riesgos/mala-comprension-objetivos}

\input{capitulos/gestion-proyecto/riesgos/falta-disponibilidad-phi}

\input{capitulos/gestion-proyecto/riesgos/no-adquisicion-phi}

% riesgos de exposicion media
\paragraph{Riesgos de exposición \textit{media}} Estos riesgos pueden producirse con una probabilidad e impactos intermedios, por lo que el equipo del proyecto debe prestar atención ante su posible aparición.
\input{capitulos/gestion-proyecto/riesgos/retrasos-burocraticos}

\input{capitulos/gestion-proyecto/riesgos/falta-disponibilidad-tutores}

\input{capitulos/gestion-proyecto/riesgos/identificacion-riesgos-incorrecta}

\paragraph{Riesgos de exposición \textit{media-baja}} Estos riesgos suponen un impacto menor o son menos probables que los riesgos de exposición media, por lo que no suponen una gran amenaza en el transcurso del proyecto.

\input{capitulos/gestion-proyecto/riesgos/baja-prolongada-autor}

\input{capitulos/gestion-proyecto/riesgos/falta-informacion-clave}

\input{capitulos/gestion-proyecto/riesgos/infeccion-virus}

\input{capitulos/gestion-proyecto/riesgos/infeccion-virus-phi}

\input{capitulos/gestion-proyecto/riesgos/perdida-soportes-informaticos}

%riesgos de exposicion baja
\paragraph{Riesgos de exposición \textit{baja}} Estos riesgos son muy improbables o su impacto es ínfimo en relación al resto de riesgos identificados. De darse, deberían ser atendidos, aunque no se suponen como críticos para el desarrollo del proyecto.
\input{capitulos/gestion-proyecto/riesgos/codigo-implementado-incorrecto}

\input{capitulos/gestion-proyecto/riesgos/resultados-benchmark-irrelevantes}

\input{capitulos/gestion-proyecto/riesgos/implementacion-benchmarks-inadecuados}

\paragraph{Diagramas de Ishikawa}
Los diagramas de Ishikawa, o de espina de pescado, sirven para representar las causas o motivos de un hecho. En este proyecto se realizarán diagramas de Ishikawa para analizar los riesgos con una exposición considerada como alta, lo cual incluye a los riesgos \refrsg{rsg:planificacion-incorrecta}, \refrsg{rsg:duracion-benchmarks} y \refrsg{rsg:falta-formacion}, cuyos diagramas se pueden consultar en las figuras \ref{fig:ishikawa-rsg1}, \ref{fig:ishikawa-rsg2} y \ref{fig:ishikawa-rsg3}.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\linewidth]{figuras/gestion-proyecto/Ishikawa/Ishikawa_rsg1}
	\caption{Diagrama de Ishikawa para el riesgo \refrsg{rsg:planificacion-incorrecta}.}
	\label{fig:ishikawa-rsg1}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=\linewidth]{figuras/gestion-proyecto/Ishikawa/Ishikawa_rsg2}
	\caption{Diagrama de Ishikawa para el riesgo \refrsg{rsg:duracion-benchmarks}.}
	\label{fig:ishikawa-rsg2}
\end{figure}

\begin{figure}[!h]
	\centering
	\includegraphics[width=\linewidth]{figuras/gestion-proyecto/Ishikawa/Ishikawa_rsg3}
	\caption{Diagrama de Ishikawa para el riesgo \refrsg{rsg:falta-formacion}.}
	\label{fig:ishikawa-rsg3}
\end{figure}
