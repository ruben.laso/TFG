\section{Gestión de costes} \label{sec:analisis-costes}
En la gestión de costes se pretende establecer una línea base que determine de manera fiable los costes económicos, que supone la realización de un proyecto. En este tipo de análisis es necesario tener en cuenta todos los elementos que afectan al presupuesto del proyecto, contando tanto los costes del personal involucrado como la compra del material necesario.

Para realizar de manera correcta el análisis de costes que requiere su gestión, hay que diferenciar dos tipos de costes:

\begin{itemize}
	\item Costes directos: son aquellos costes propios del proyecto de una forma clara, como pueden ser sueldos de los trabajadores, equipamiento adquirido para la realización del proyecto, materiales fungibles, etc.
	
	\item Costes indirectos: son los costes que, sin formar parte del proyecto de forma explícita, son requeridos para poder realizar el proyecto como suministros de electricidad, agua, conexión a internet, costes de administración, etc.
\end{itemize}

Una vez realizada esta clasificación, se puede proceder a realizar el análisis con cierta seguridad en que el resultado va a ser confiable.

\subsection{Costes directos}
En cuanto a los costes de personal, para una persona con el perfil de ingeniero, que trabaje como técnico superior a la investigación, y suponiendo que trabaja a tiempo completo realizando 37.5 horas por semana, debería obtener un salario bruto mensual de 1,477.13\ \euro{} \cite{calculadora-contratos-usc}, Lo cual supone un coste por hora de:

\begin{align*}
	\frac{1,477.13}{37.5 \times 4} = \frac{1,477.13}{150} = 9.84\ \text{\euro/hora} 
\end{align*}

Si tenemos en cuenta que la realización de un \tfg{} se supone en 412.50 horas de trabajo por parte del alumno, el coste que supondría, de estar contratado, sería de:

\begin{align*}
	9.84 \times 412.50 = 4,059.00\ \text{\euro}
\end{align*}

Por otro lado, hay que tener en cuenta el coste del trabajo de los tutores, que se calculan sobre unas 20 horas por tutor durante toda la realización del TFG. Si atendemos a los datos cedidos por los propios tutores del TFG en relación a un proyecto de investigación de este mismo año, tenemos que el coste asociado al Prof. \jccabaleiro{} es de 36 \euro/hora, mientras que para el Prof. \ffrivera{} es de 40 \euro/hora. Esto implica un coste de los tutores estimado en:

\begin{align*}
	20 \times 36.00 + 20 \times 40.00 = 720.00 + 800.00 = 1,520.00\ \text{\euro}
\end{align*}

Una vez cubiertos los costes humanos, es necesario contabilizar los costes materiales, que en este caso son tanto o más significativos que los anteriores. Para estimar el coste y el rendimiento de los elementos se utiliza la fórmula de la expresión~\ref{eq:coste-economico}, donde $c$ representa el coste imponible al proyecto, $c_{\text{inicial}}$ es el coste inicial del producto, $t_{\text{vida}}$ representa el tiempo de vida estimado para el material y $t_{\text{uso}}$ es el tiempo que va a ser utilizado en el proyecto.

\begin{equation} \label{eq:coste-economico} 
	c = \frac{c_{\text{inicial}}}{t_{\text{vida}}} \times t_{\text{uso}}
\end{equation} 

En esta categoría de materiales, los más destacados e influyentes en el presupuesto son:
\begin{itemize}
	\item Computador personal de desarrollo: computador utilizado por el autor de este TFG, que aunque no tiene uso exclusivo para este TFG, sí supone la principal herramienta de trabajo (en cuanto a tiempo de uso) en la realización de este proyecto. Con un coste inicial de $1,300$ \euro, se le supone una vida útil de 4 años (48 meses), con un uso en el proyecto de 5 meses, lo que implica:
	\begin{align*}
		c = \frac{1,300}{48} \times 5 = 135.41\ \text{\euro}
	\end{align*}
	
	\item Servidor del Intel Xeon Phi: servidor donde se alojará el Intel Xeon Phi y todos los componentes necesarios para su funcionamiento. El uso de este producto no va a ser exclusivo para este proyecto. Su compra ha supuesto un total de $9,966.17$ \euro, con una vida útil estimada en 3 años (36 meses) y 4 meses de utilización dentro de este proyecto. El coste que supone es el siguiente:
	\begin{align*}
		c = \frac{9,966.17}{36} \times 4 = 1107.35\ \text{\euro}
	\end{align*}
\end{itemize}

Además, todo el software utilizado para realizar este proyecto es libre y/o gratuito, por lo que no supone coste adicional. El resumen de los costes generados en este proyecto se muestra en la tabla~\ref{tab:costes-directos}.

\input{capitulos/gestion-proyecto/gestion-costes/costes-directos} 

\subsection{Costes indirectos}
Los costes indirectos son aquellos que, sin ser producidos directamente por el desarrollo del proyecto, son necesarios para finalizarlo. En este tipo de gastos se pueden incluir los gastos de proveedores de servicios como  local de trabajo, electricidad, agua, internet, etc. Estos costes suelen calcularse respecto de los costes directos, tasándolos en un tanto por ciento de éstos. 

La Xunta de Galicia prevé unos costes indirectos de hasta un 20\% para los proyectos de investigación \cite{costes-indirectos-Xunta}, por lo que la estimación aquí mostrada se adecuará a esta cifra. En concreto, para los proyectos informáticos realizados en la USC se estima que los costes indirectos suponen un 20 \% de los costes directos, lo que aplicado a este proyecto resulta en $1,364.35$ \euro.

\subsection{Resumen de costes}
Una vez analizados todos los tipos de costes, en la tabla~\ref{tab:resumen-costes} se puede ver un desglose del presupuesto estimado del proyecto incluyendo tanto los costes directos como la estimación de los costes indirectos.

\input{capitulos/gestion-proyecto/gestion-costes/resumen-costes} 