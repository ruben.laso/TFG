\subsection{Condiciones de carrera o \textit{race conditions}} \label{sec:carrera-critica}
En computación, se conoce como \emph{condición de carrera} aquella situación en la que dos procesos o hilos están leyendo y escribiendo datos en memoria compartida y el resultado de las operaciones puede depender del orden en el cual se han ejecutado los procesos o hilos \cite{tanenbaum2009sistemas}.

Un ejemplo de condición de carrera podría ser el de una cola de impresión donde los procesos añadan sus documentos a la cola de forma independiente, sin tener en cuenta que hay más procesos que tienen acceso a la cola. La cola de impresión tendrá varias variables, como \texttt{out}, que apuntaría al siguiente fichero a imprimir y, por tanto, salir de la cola, e \texttt{in}, que apuntaría a primera posición libre de la cola.

Supongamos dos procesos \textit{A} y \textit{B}, los cuales quieren imprimir  prácticamente al mismo tiempo los documentos \texttt{doc\_A} y \texttt{doc\_B} respectivamente y que los valores de las variables de la cola son \texttt{out} = 0 e \texttt{in} = 4, tal y como se muestra en la figura~\ref{fig:condicion-carrera}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/diseno-benchmarks/condicion-carrera/condicion-carrera}
	\caption{Situación inicial de nuestra cola de impresión.}
	\label{fig:condicion-carrera}
\end{figure}

En un primer momento ambos verían que la siguiente posición libre en la cola de impresión es la 4. Imaginemos que el proceso \textit{A} es el primero que decide imprimir su documento, por lo que en primer lugar leería el valor de \texttt{in} para saber en qué posición de la cola tiene que escribir (4 en nuestro ejemplo), después escribiría la ruta al fichero del documento \texttt{doc\_A} para que la impresora haga su trabajo. Desafortunadamente, puede suceder un cambio de contexto hacia el proceso \textit{B} antes de que complete el último paso, actualizar el valor de \texttt{in} para que apunte a la siguiente posición realmente libre de la cola, por lo que la situación quedaría tal y como se puede ver en la figura~\ref{fig:condicion-carrera2}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/diseno-benchmarks/condicion-carrera/condicion-carrera2}
	\caption{Situación de nuestra cola de impresión justo tras el cambio de contexto del proceso \textit{A} al proceso \textit{B}.}
	\label{fig:condicion-carrera2}
\end{figure}

Como el proceso \textit{A} no ha tenido tiempo de actualizar el valor de \texttt{in}, el proceso \textit{B} lee que la siguiente posición libre en la cola es la 4, justo donde acaba de escribir su documento el proceso \textit{A}, de forma que elimina \texttt{doc\_A} para escribir \texttt{doc\_B}, y finalmente actualiza el valor de \texttt{in}, quedando la cola de impresión como se muestra en la figura~\ref{fig:condicion-carrera3}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/diseno-benchmarks/condicion-carrera/condicion-carrera3}
	\caption{Situación de nuestra cola de impresión tras el trabajo del proceso \textit{B}.}
	\label{fig:condicion-carrera3}
\end{figure}

Este tipo de situaciones tienen varios problemas como el hecho de que el proceso \textit{B} ha sobrescrito el valor introducido por el proceso \textit{A}. En este caso la solución sería tan sencilla como que el proceso \textit{A} volviera a colocar su documento en la cola (con la esperanza de que no sufra otro desafortunado cambio de contexto), pero en la mayoría de situaciones no se pueden volver a realizar las tareas, y lo que es aún peor, el proceso \textit{A} ni siquiera se daría cuenta de lo que ha sucedido y podría seguir su ejecución con total normalidad.

Las condiciones de carrera se manifiestan con muy poca frecuencia, pero en el momento en que lo hacen pueden llevar a resultados incluso catastróficos en la ejecución de un programa. Por ello, es necesario que los programadores tengan en cuenta estas situaciones y pongan barreras para impedir que se den, pues hemos de recordar que ``si algo puede salir mal, lo hará''.

\subsubsection{Regiones críticas y exclusión mutua} \label{sec:mutex}
Cuando se dan situaciones de carrera, lo primero que debería hacer un programador responsable y que no deja nada al azar es detectar en qué punto de su código se producen estas condiciones de carrera. Así pues, conocemos como \emph{regiones críticas} a las secciones de código donde se pueden producir carreras críticas y a las que debe prestarse especial atención en los programas con compartición de recursos.

Para solventar los problemas de las condiciones de carrera que se producen por la compartición de recursos entre varios procesos o hilos, hemos de evitar que dos de estos sean capaces de acceder a las regiones críticas simultáneamente, es decir, \emph{exclusión mutua}.

Habitualmente para resolver estos problemas se opta por la implementación de \textit{mutex} o regiones atómicas en las que sólo pueda entrar un proceso al mismo tiempo, asegurando así que las tareas que ha empezado un proceso serán finalizadas antes de que otro pueda interferir en el resultado. En la figura~\ref{fig:mutex} se muestra un diagrama de secuencia que ilustra la correcta ejecución de dos procesos que comparten recursos y en los que la exclusión mutua ha sido bien implementada.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=\linewidth]{figuras/diseno-benchmarks/mutex}
	\caption{Diagrama de secuencia representando la exclusión mutua.}
	\label{fig:mutex}
\end{figure}

En esta figura, el proceso A es el primero en entrar en la región crítica en $\text{t}_1$, de esta forma, cuando el proceso B quiere entrar en su región crítica ($\text{t}_2$), debe bloquearse (o realizar otros trabajos) y esperar a que el proceso A acabe sus tareas dentro de la región crítica. Una vez que el proceso A ha acabado su trabajo, el proceso B puede entrar en la región crítica ($\text{t}_3$) y realizar sus propias tareas dentro de ella sin que otros procesos ataquen dicha región hasta que la abandone en $\text{t}_4$.

Si conseguimos que esta solución se adapte a nuestro problema, conseguiremos evitar de forma eficaz las condiciones de carrera. Por otro lado, no todo es un camino de rosas, pues mientras el Proceso B espera a que A salga de la región crítica, está detenido o, en el altamente improbable mejor de los casos, está realizando otras tareas. Esto provoca una pérdida del paralelismo dentro de la región crítica. Por este motivo, el programador ha de intentar buscar métodos alternativos para evitar generar estas regiones críticas con el fin de que no se produzca una serialización del código en dicha región.

%Examinando el código del problema que aquí estamos implementando, el producto de matrices, estas condiciones de carrera pueden darse cuando se paraleliza el bucle correspondiente al índice $k$, pues este no influye en la dirección donde se escribe en la matriz resultado, por lo que habrá múltiples \textit{threads} intentando escribir en la misma posición de memoria de forma simultánea. Además, aunque sean poco habituales las manifestaciones de las condiciones de carrera, con que se den una única vez (algo frecuente dado el enorme número de cálculos), invalidan por completo la ejecución del programa.
%
%\lstinputlisting[label=lst:carrera, firstline=88, lastline=94]{../../src/ijk_index_location_benchmarks/matrix_multiplication_basic.c}

