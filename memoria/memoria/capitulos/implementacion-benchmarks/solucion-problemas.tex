\section{Control de las condiciones de carrera y \textit{false sharing}} \label{sec:solucion-problemas}
Una vez visto que los problemas de las condiciones de carrera y \textit{false sharing} se pueden dar dentro del código de los \textit{benchmarks}, el siguiente paso es estudiar cómo afectaría a las pruebas y si son aceptables sus manifestaciones y consecuencias.

\subsection{Control de las condiciones de carrera} \label{sec:control-condiciones-carrera}
Examinando el código del problema que estamos implementando (código~\ref{lst:carrera}), el producto de matrices, las condiciones de carrera pueden darse cuando se paraleliza el bucle correspondiente al índice $k$, pues este no influye en la dirección donde se escribe en la matriz resultado, por lo que habrá múltiples \textit{threads} intentando escribir en la misma posición de memoria (determinada por $i$ y $j$) de forma simultánea. 

Además, aunque sean poco habituales las manifestaciones de las condiciones de carrera, con que se den una única vez (algo muy posible dado el enorme número de cálculos), invalidan por completo la ejecución del programa.

\lstinputlisting[label=lst:carrera, firstline=83, lastline=89, caption=Multiplicación de matrices implementado en C.]{../../src/Cache_benchmarks/matrix_multiplication_ijk.c}

Por fortuna, OpenMP provee herramientas para implementar la exclusión mutua dentro del código paralelo. Esto se puede realizar mediante la directiva \texttt{\#pragma omp atomic}, de esta forma, el código quedaría tal y como se muestra en el código~\ref{lst:pragma-atomic}.

\lstinputlisting[label=lst:pragma-atomic, firstline=82, lastline=90, caption=Multiplicación de matrices implementado en C variando el orden de los lazos y controlando las condiciones de carrera.]{../../src/Cache_benchmarks/matrix_multiplication_kij.c}

Esta directiva permite a múltiples hilos actualizar el valor de una variable de forma segura \cite{openMP-atomic}, es decir, el conjunto de instrucciones para realizar esa asignación se ejecutan sin cambios de contextos y sin que otros procesos intercalen su propia ejecución. Para hacer esto se apoya en las capacidades del hardware que permiten operaciones atómicas. Esta directiva sólo se aplica a la asignación que la sigue inmediatamente, por lo que sólo afecta a una sentencia del código.

Otra alternativa a \texttt{\#pragma omp atomic} sería \texttt{\#pragma omp critical} \cite{openMP-critical}, que implementa un \textit{mutex} convencional, donde un único \textit{thread} puede estar activo dentro de la región crítica. El problema es que esta última alternativa añade un \textit{overhead} mayor a nuestro programa que el uso de \texttt{atomic}, por lo que nos hemos decantado por utilizar la directiva atómica, puesto que su uso, \textit{a priori}, se ajusta perfectamente a nuestro programa.

\subsubsection{Problemas del control de las carreras críticas}
Como ya se ha mencionado en la sección~\ref{sec:mutex}, la implementación de la exclusión mutua puede conllevar la serialización del código. En nuestro programa, la única instrucción de la región crítica es la asignación en la matriz resultado. Esta, a su vez, es la única sentencia dentro del lazo más interno.

Si consideramos establecer esta operación como atómica, vemos que los \textit{threads} escribirán sus resultados parciales de forma secuencial, de una forma similar a como lo haría un único hilo. De hecho, es probable que el control de la atomicidad de la operación suponga un tiempo tan grande que incluso se pierda rendimiento con respecto del programa secuencial. En la figura~\ref{fig:mutex-matrix} se puede ver un diagrama de secuencia que ilustra esta situación.
En esta figura, el programa paralelizado tiene cuatro hilos, los cuales se disputan el entrar en la región crítica. Al estar el \textit{mutex} bien implementado, ninguno entra en la región crítica si ya hay otro hilo activo dentro de dicha región. Sin embargo, el control de la exclusión mutua tiene un pequeño \textit{overhead}, el cual se representa en los intervalos de tiempo $[\text{t}_2, \text{t}_3]$, $[\text{t}_4, \text{t}_5]$ y $[\text{t}_6, \text{t}_7]$. Por culpa de este \textit{overhead} y de la ``secuencialización'' del código por haber implementado la exclusión mutua, el tiempo de ejecución del programa paralelo representado por el intervalo $[\text{t}_1, \text{t}_8]$, es mayor que la del programa secuencial al que corresponde el intervalo $[\text{t}_{9}, \text{t}_{13}]$.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.9\linewidth]{figuras/diseno-benchmarks/mutex-matrix}
	\caption{Diagrama de secuencia para la ejecución de un programa paralelo con exclusión mutua y uno secuencial.}
	\label{fig:mutex-matrix}
\end{figure}

Pongámonos en el hipotético caso de que el control de la exclusión mutua no tiene ningún coste en el rendimiento. Dado que el tiempo de computación de una multiplicación de matrices de orden $32,000$ con 68 \textit{threads} que pueden aplicar una paralelización sin control de las condiciones de carrera puede suponer unas 6 ó 7 horas, si tenemos en cuenta el control de las carreras críticas este tiempo de computación se podría disparar hasta las $7 \text{ h} \times 68 = 476 \text{ h} \approx 20 \text{ días}$.

Dado que el tiempo para realizar el proyecto es limitado, hay tests más interesantes que realizar que una multiplicación de matrices pseudo-secuencial y dado que hay más usuarios que quieren utilizar la máquina objeto de estudio, se ha decidido que los \textit{benchmarks} que impliquen el control de las carreras críticas no van a ser ejecutados, pues hay otro tipo de anidamientos con los que se consiguen patrones similares de acceso a memoria y su tiempo de ejecución es demasiado alto como para resultar viable, además de que no sería posible llevar a un punto de estrés a la máquina si todos los accesos a memoria se hacen de una forma prácticamente secuencial.

\subsection{Control del \textit{false sharing}}
Como se ha explicado en la sección~\ref{sec:false-sharing} (pág.~\pageref{sec:false-sharing}), el \textit{false sharing} no invalida los resultados de un programa tal y como podría suceder con las condiciones de carrera, sino que puede provocar una pérdida de rendimiento significativa.

Dado que el resultado sigue siendo correcto, los \textit{benchmarks} que se ejecuten y en los cuales se den fenómenos de falsa compartición serán válidos. Por otro lado, también se ha de tener en cuenta cuándo se dan, para no obtener conclusiones precipitadas de sus datos, sino que se han de analizar cuidadosamente para llegar a conclusiones válidas.