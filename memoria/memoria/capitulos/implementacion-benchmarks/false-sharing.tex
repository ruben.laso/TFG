\subsection{Falsa compartición o \textit{false sharing}} \label{sec:false-sharing}
Para presentar debidamente qué es la falsa compartición o \textit{false sharing}, primero debemos recordar cómo funciona la memoria caché.
La memoria caché es una memoria de muy baja latencia y capacidad, y la que más cerca se sitúa de los \textit{cores} de un procesador. A diferencia de los propios registros del procesador, este tipo de memoria no trabaja con datos sueltos, sino que trabaja con lo que se conoce como líneas caché, las cuales tienen una capacidad que comúnmente es de 64 bytes, como es el caso del Intel Xeon Phi.

En las líneas caché se almacenan conjuntos de datos que, a su vez, se alojan contiguamente en la memoria principal. Cuando el procesador necesita acceder a un dato de la memoria, el primer lugar al que acude es a la memoria caché de primer nivel, de estar ahí, directamente coge el dato de la caché L1, ahorrándose el tener que ir a memoria principal (con la penalización en tiempo que ello conllevaría). En caso de que el dato no esté, se acudirá a los siguientes niveles de caché hasta que se encuentre, que en el peor de los casos estará en memoria principal (si exceptuamos la posibilidad de que la dirección de memoria del dato no exista o no sea válida, lo que nos llevaría a un fallo en la ejecución del programa) o, si hay fallo de página, en el disco duro.

Dado que el procesador no tiene la necesidad de ir a memoria para leer un dato, tampoco tiene necesidad de acceder en el caso de una escritura, sino que puede modificar los datos directamente en la caché, en cuyo caso la línea caché donde se almacena el dato sobrescrito será marcada como \emph{modificada}. 

En un procesador con un único núcleo o con memoria caché totalmente privada, esto no supone ningún problema, pero cuando hay múltiples \textit{cores} accediendo a memoria caché la cosa se complica.
Imaginemos el caso en el que tenemos a dos hilos (\textit{thread A} y \textit{thread B}) ejecutándose en dos \textit{cores} diferentes de manera concurrente calculando diferentes elementos contiguos (\texttt{var\_1} y \texttt{var\_2}) de un mismo vector y que éstos se encuentran en la misma línea caché, tal y como se muestra en la figura~\ref{fig:false-sharing}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/diseno-benchmarks/false-sharing/false-sharing}
	\caption{Estado inicial de la línea caché de nuestro ejemplo.}
	\label{fig:false-sharing}
\end{figure}

En esta situación todavía no se han realizado modificaciones en la línea caché desde que se trajo de memoria principal, por lo que los hilos y procesos que accedan a ella pueden leer los datos que contiene sin preocupación. Supongamos ahora, que el \textit{thread A} es el primero que realiza sus cálculos y sobrescribe el elemento \texttt{var\_1}, poniéndolo a 9, por ejemplo. La línea caché será marcada como \emph{modificada} y se dará la situación de la figura~\ref{fig:false-sharing2}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\linewidth]{figuras/diseno-benchmarks/false-sharing/false-sharing2}
	\caption{Estado \emph{modificado} de la línea caché, debido a que se ha cambiado el valor del elemento \texttt{var\_1}.}
	\label{fig:false-sharing2}
\end{figure}

Supongamos ahora, que el \textit{thread B}, para ejecutar sus cálculos necesita leer el contenido de \texttt{var\_2}. En principio no le debería afectar el hecho de que \texttt{var\_1} haya sido sobrescrita, pero al ser marcada la línea caché como \emph{modificada}, es necesario almacenar los valores actuales de la línea caché y volver a cargarla de memoria, por lo que volvería a un estado \emph{no modificado}, antes de que el \textit{thread B} pueda realizar su trabajo.

Esta situación que se manifiesta en los sistemas multiprocesador y multinúcleo, en la que una línea caché necesita ser recargada de memoria por haber sido modificado algún dato de la dicha línea es conocida como falsa compartición o \textit{false sharing}.

El mayor efecto de la falsa compartición, a pesar de ser difícil de medir o estimar su impacto \cite{false-sharing}, es la pérdida de rendimiento, pues no sólo hay que volver a cargar la línea caché, sino que también hay que salvar los cambios realizados en dicha memoria.