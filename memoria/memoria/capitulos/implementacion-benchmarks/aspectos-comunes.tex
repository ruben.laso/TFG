\subsection{Aspectos comunes} \label{sec:aspectos-comunes}
Para empezar, veremos los aspectos comunes de estos \textit{benchmarks}, para centrarnos en las secciones posteriores en los aspectos singulares de cada implementación.

\subsubsection{Librerías estándar}
La mayoría de las librerías utilizadas son las ``propias'' del lenguaje C recogidas dentro del estándar de ANSI C11. En este conjunto, podemos citar \texttt{stdio}, \texttt{stdlib} y \texttt{time}, que nos permiten reservar memoria, realizar operaciones de entrada/salida, medir tiempos de ejecución, etc.

\subsubsection{Librerías propias} 
Por motivos de diseño, y sobre todo para facilitar el mantenimiento del código en caso de errores, se ha creado una librería auxiliar \texttt{auxiliaries}. El objetivo de la librería es aislar y centralizar en un mismo fichero la reserva de memoria, la inicialización de las matrices y liberación de memoria, para así, en caso de precisar cambios en estos métodos sólo sea necesario modificar un único fichero.



%\lstinputlisting[label=lst:struct-matrix, firstline=6, lastline=12, caption=Definición de la estructura \texttt{st\_matrix} dentro del fichero \texttt{auxiliaries.h}.]{../../src/Cache_benchmarks/auxiliaries.h}
En esta librería se define una estructura matriz (a la que se referenciará como \texttt{matrix}) sobre la cual nos apoyaremos en las funciones que provee la librería \texttt{auxiliaries}:
%Esta estructura (a la que se referenciará como \texttt{matrix}) será utilizada por las siguientes funciones:

\begin{itemize}
	\item \texttt{bool initialize\_matrix (int number\_of\_rows, int number\_of\_columns, matrix * \_matrix)}: Esta función reserva memoria para la matriz y sus campos, en función del número de filas y columnas especificadas. El valor de los elementos de la matriz es indefinido. En caso de éxito, devuelve \texttt{true}, en caso contrario, \texttt{false}.
	
	\item \texttt{bool initialize\_matrix\_to\_cero (matrix \_matrix)}: Recorre todos los elementos de la matriz \texttt{matrix} y los inicializa a cero. En caso de éxito, devuelve \texttt{true}, en caso contrario, \texttt{false}.
	
	\item \texttt{bool initialize\_matrix\_by\_indexes (matrix \_matrix)}: Recorre todos los elementos de la 
	matriz \texttt{matrix} y los inicializa tal que: $m_{ij} = (i \times j) \div n$, donde $i$ es el índice de la file, $j$ el de la columna y $n$ el número de elementos de la matriz. En caso de éxito, devuelve \texttt{true}, en caso contrario, \texttt{false}.
	
	\item \texttt{bool print\_matrix\_file (char * filename, matrix \_matrix)}: Imprime en el fichero \texttt{filename} el contenido de la matriz \texttt{matrix} en ese momento. En caso de éxito, devuelve \texttt{true}, en caso contrario, \texttt{false}.
	
	\item \texttt{void destroy\_matrix (matrix * \_matrix)}: Libera la memoria reservada para la estructura \texttt{matrix}.
	
\end{itemize}

%El fichero de cabecera de la librería se puede consultar en el Listing \ref{lst:auxiliaries-header}, en el Apéndice \ref{sec:auxiliaries-library-ap}.

Por último, se ha de mencionar que esta librería es modificada en función del modo de memoria que se vaya a utilizar en la máquina, pues utilizará o no determinadas funciones para alojar datos en la memoria MCDRAM (ver secciones~\ref{sec:implementacion-cache} y \ref{sec:implementacion-flat}, págs.~\pageref{sec:implementacion-cache} y \pageref{sec:implementacion-flat}).

\subsubsection{Código de los \textit{benchmarks}}
El código de los \textit{benchmarks}, es en general igual para todos los programas. De hecho, los programas de pruebas implementados sólo se diferencian entre sí en dos cosas, por un lado, la forma de inicializar la memoria (que realmente se realiza dentro de la librería \texttt{auxiliaries}), y por otro, el orden en el que se ejecutan los lazos de la multiplicación de matrices. En esta sección se comentan los detalles más importantes del código de las pruebas fuera de estos matices.

En la cabecera del programa se incluyen las librerías necesarias para que pueda ejecutarse correctamente, así como definición de variables globales que serán de utilidad en el proceso de depuración, así como las constantes para imprimir a fichero los resultados de las matrices.

Un elemento importante de estos programas son las opciones pasadas como argumentos al programa. La lista de argumentos aceptados es la siguiente:

\begin{enumerate}
	\item Tamaño de la matriz (\texttt{-s orden} o \texttt{--size orden}): \texttt{orden} ha de ser un entero positivo, que determinará el orden $n$ de la matriz. Es decir, se generarán matrices de tamaño $n \times n$.
	
	\item Número de \textit{threads} (\texttt{-t numero\_threads} o \texttt{--threads numero\_threads}): \texttt{numero\_de\_threads} ha de ser un entero positivo. Determinará el número de hilos que se ejecutarán concurrentemente en la región de código correspondiente al producto matricial.
	
	\item Impresión de matrices (\texttt{-p} o \texttt{--print}): de existir este argumento, al finalizar la ejecución del programa, se imprimirán las matrices en fichero.
	
	\item Modo depuración (\texttt{-v} o \texttt{--verbose}): de incluir esta opción, se imprimirán diferentes mensajes durante la ejecución del programa que facilitan las tareas de depuración del código.
\end{enumerate}

Para la implementación de estas opciones de ejecución se ha utilizado la librería \texttt{getopt}~\cite{getopt}. Con las funciones de esta librería, reconocer las opciones que se pasan al programa en tiempo de ejecución se convierte en algo sencillo. En primer lugar, se deben definir mediante estructuras de tipo \texttt{option} y \textit{strings} las opciones que se aceptarán en el programa.
% (ver código~\ref{lst:getopt-definition}).

%\lstinputlisting[label=lst:getopt-definition, firstline=18, lastline=25, caption=Definición del conjunto de opciones admitidas en el programa.]{../../src/Cache_benchmarks/matrix_multiplication_ijk.c}

Posteriormente, en la función \texttt{process\_arguments}, se procesan las opciones introducidas cambiando los valores de las variables globales del programa conforme a los valores introducidos por el usuario.

%\lstinputlisting[label=lst:getopt-processing, firstline=120, lastline=144, caption=Procesado del conjunto de opciones admitidas en el programa.]{../../src/Cache_benchmarks/matrix_multiplication_ijk.c}

Después de procesar los argumentos del programa, se inicializan las matrices y estructuras necesarias para llevar a cabo el \textit{benchmark}. Una vez inicializados todos estos elementos se puede proceder a la multiplicación de matrices en sí (ver código~\ref{lst:matrix-multiplication}).

\lstinputlisting[label=lst:matrix-multiplication, firstline=75, lastline=92, caption=Multiplicación de matrices con anidamiento \textit{ijk}.]{../../src/Cache_benchmarks/matrix_multiplication_ijk.c}

Justo antes de proceder a la multiplicación, se inicializan tres punteros que optimizarán el acceso a los elementos de las matrices. Recordemos que las matrices están contenidas en estructuras, las cuales funcionan de forma similar a como lo haría un puntero. Por ejemplo, supongamos la estructura \texttt{libro}, con los campos \texttt{autor} y \texttt{titulo}, si quisiéramos acceder al campo \texttt{titulo}, antes habría que resolver la dirección donde comienza la estructura, y posteriormente ver el \textit{offset} que tiene el campo \texttt{titulo} para dirigirnos a la dirección de memoria donde se almacena el título del libro. Algo similar pasa con las estructuras de nuestras matrices, por lo que se ha decido usar punteros directos a los \textit{doubles} que forman la matriz.

Por otro lado, en el direccionamiento de los elementos dentro del producto matricial siempre se utilizará el índice $i$ para determinar la fila del elemento en cuestión. Esto provoca que este índice se utilice siempre de la forma $i \times n$. Para mejorar la eficiencia y el rendimiento del programa y así ahorrar multiplicaciones, lo que se hace es definir el valor máximo que puede tomar $i$, el cual sería $n^2$. De esta forma, en cada iteración del bucle realizaremos la operación $i = i + n$, ahorrándonos en cada paso dos multiplicaciones, y sustituyéndolas por una única suma (ver códigos~\ref{lst:index-i-values} y \ref{lst:index-i-values-optimized}). Además, se gana en legibilidad del código. Con estas medidas, se ha notado una reducción en los tiempos de ejecución de hasta el 50\%.

\lstinputlisting[label=lst:index-i-values, caption=Producto matricial con un uso no optimizado de los valores de $i$.]{codigo/matrix-multiplication-i-values.c}

\lstinputlisting[label=lst:index-i-values-optimized, caption=Producto matricial con un uso optimizado de los valores de $i$.]{codigo/matrix-multiplication-i-values-optimized.c}

En relación al direccionamiento de elementos y a cómo se sitúan en la memoria, se ha decidido implementar las matrices como un único \textit{array} unidimensional en lugar de como un \textit{array} de \textit{arrays}. Esto se debe a que al tener todos los elementos contiguos en memoria nos podemos beneficiar de la localidad de los datos.

Una implementación ``tradicional'' sería la mostrada en el código~\ref{lst:implementacion-2mallocs}, donde primero se reserva espacio para $n$ punteros (que representarían cada una de las filas), y posteriormente, para cada puntero se reservan $n$ elementos. 

\lstinputlisting[label=lst:implementacion-2mallocs, firstline=0, lastline=100, caption=Reserva de memoria ``tradicional'' de una matriz en C.]{codigo/implementacion-2mallocs.c}

De esta forma, conseguiríamos algo similar a lo que se puede ver en la figura~\ref{fig:implementacion-2mallocs}, donde cada una de las filas de la matriz tiene sus elementos contiguos en memoria, lo que es bueno en términos de localidad, pues aprovechamos las líneas cachés y el \textit{prefetching} en los accesos a elementos de la misma fila. Sin embargo, en el momento en que queremos acceder a un elemento de otra fila, tendremos que cambiar a otra dirección que puede estar bastante alejada, echando por tierra todo el trabajo realizado por la memoria caché y el sistema de \textit{prefetching}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.35\linewidth]{figuras/implementacion/localidad-malloc/implementacion-2mallocs}
	\caption{Representación gráfica de la ubicación de los elementos en memoria en un \textit{array} de \textit{arrays}.}
	\label{fig:implementacion-2mallocs}
\end{figure}

Esta implementación tiene la ventaja de que se podrán direccionar los elementos de la forma \texttt{matriz[fila][columna]}, pero ya veremos que esto se puede conseguir de otras formas.

Con vistas a optimizar la localidad de los datos, se ha decidido implementar toda la matriz como un único \textit{array} de elementos, tal y como se muestra en la figura~\ref{fig:implementacion-1malloc}. El código para realizar la reserva de memoria de esta opción se muestra en el código~\ref{lst:implementacion-1malloc}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.6\linewidth]{figuras/implementacion/localidad-malloc/implementacion-1malloc}
	\caption{Representación gráfica de la ubicación de los elementos en memoria de una matriz implementado como un único \textit{array}.}
	\label{fig:implementacion-1malloc}
\end{figure}

\lstinputlisting[label=lst:implementacion-1malloc, firstline=0, lastline=100, caption=Reserva de memoria de una matriz como un único \textit{array} en C.]{codigo/implementacion-1malloc.c}

Utilizando esta forma de situar los elementos de una matriz, todos están contiguos en memoria, desde el primero hasta el último, aprovechando así todas las ventajas de la memoria caché y el \textit{prefetching}.
La forma de referenciar ahora los datos sería tal que \texttt{matriz[fila * n + columna]}, lo cual es menos legible que la mostrada con la alternativa ``tradicional''.

Si se quisiera seguir utilizando la notación tradicional a pesar de esta localidad de los datos podría hacerse como se muestra en el código~\ref{lst:fix-mallocs}.

\lstinputlisting[label=lst:fix-mallocs, firstline=0, lastline=100, caption=Código para utilizar el direccionamiento tradicional con la implementación secuencial.]{codigo/fix-mallocs.c}

Se hace apuntar a cada una de las filas, al primer elemento de cada una de ellas, que se situará en la posición $f \times n$, donde $f$ es el número de la fila, y $n$ el número de elementos contenidos en cada fila. De esta forma podremos direccionar los elementos de la forma \texttt{matrix\_tradicional[fila][columna]}. Sin embargo, experimentalmente se ha comprobado que esta forma de direccionamiento es ligeramente más lenta que la opción menos legible, por eso es esta última la que se ha utilizado.

Ya para finalizar el programa, se imprimen en fichero las matrices (si el usuario ha introducido tal orden), se imprimen los tiempos de ejecución y se libera toda la memoria reservada.

El código completo de este \textit{benchmark} de multiplicación de matrices se muestra en el código~\ref{lst:matrix-multiplication-ijk} en el apéndice~\ref{ap:multiplicacion-matrices-ijk}.