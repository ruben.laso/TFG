\section{Procesadores \textit{manycore}} \label{sec:procesador-manycore}
En 1975, Gordon Moore (cofundador de Intel) afirmó que el número de transistores se duplicaría cada veinticuatro meses\footnote{Realmente, en 1965 afirmó que el número de transistores se duplicaría cada doce meses, pero en 1975 rectificó hacia la postura que hoy conocemos.}, la conocida como Ley de Moore. Hasta la actualidad, esta ley empírica ha sido notablemente precisa, como se puede comprobar en la figura~\ref{fig:ley-moore}, aunque en el sector se marca más como un objetivo a alcanzar que como una ley de por sí.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=\linewidth]{figuras/intel-xeon-phi/ley-moore/ley-moore}
	\caption{Evolución de los procesadores Intel respecto de la Ley de Moore.}
	\label{fig:ley-moore}
\end{figure}

El aumento en la densidad de los transistores que formula la ley de Moore implica que, en consecuencia, el tamaño de estos disminuye. Esto dio paso al Escalamiento de Dennard, ley postulada en 1974 por Robert H. Dennard. Esta ley indica que mientras el tamaño de un transistor se reduce, su densidad de potencia se mantiene constante, por lo que la potencia eléctrica está relacionada con al área del transistor, así como con el voltaje y la longitud del mismo~\cite{Dennard-Scaling}.

%la potencia de un transistor es correlativa a su tamaño, por lo que si reducimos a la mitad el tamaño del transistor, también disminuirá la potencia, por lo que se podrá aumentar la frecuencia del mismo y así aumentar su rendimiento.

Esta ley fue ampliamente aceptada hasta principios de siglo, pero alrededor del año 2006, los fabricantes se toparon con una barrera en la potencia consumida y en el calor generado dentro de los chips debido a la altísima densidad de potencia por el escalamiento de los transistores. Esto puede explicarse mediante la expresión~\ref{eq:cmos-power}.

\begin{equation} \label{eq:cmos-power}
	P = QfCV^2 + I_{fuga}
\end{equation}

La potencia $P$ consumida por un chip varía en función, por un lado, del número de transistores $Q$, la frecuencia $f$ a la que funcionen, su capacidad $C$ y su voltaje $V$ al cuadrado y, por otro lado, las corrientes de fuga $I$ (pérdidas en el chip que no efectúan ningún trabajo). Si tenemos en cuenta todos estos factores, llegamos a la conclusión de que la potencia de cada transistor escala aproximadamente entre un 0.7 y un 0.8 en cada generación, mientras que el número de transistores se duplica. Además, las corrientes de fuga aumentan exponencialmente, suponiendo una parte cada vez más importante del consumo final del procesador~\cite{Elisardo-Tema1}.

Desde el momento en que el aumento de la frecuencia ya no servía para aumentar el rendimiento, los fabricantes se vieron en la necesidad de encontrar otras vías para mejorar las prestaciones de sus productos.
La opción elegida sería la de añadir un mayor número de núcleos de procesamiento dentro de cada chip, lo cual permitía aumentar el número de operaciones que se realizaban simultáneamente, y por ende su rendimiento, sin aumentar la frecuencia del procesador. Esta apuesta por el paralelismo ya se realizó durante bastantes años en los clústers de computación de altas prestaciones, y se reafirmó hasta llegar a los procesadores que hoy conocemos como \textit{manycore}.

Este término comprende a los procesadores que pasan de tener un conjunto pequeño de \textit{cores} a decenas o cientos de núcleos. Los mayores exponentes de este paradigma de computación son los aceleradores gráficos pensados para la GP-GPU (\textit{General-Purpose Computing on Graphics Processing Units}), como son las tarjetas de NVIDIA y su tecnología CUDA, que contienen hasta miles de núcleos en cada tarjeta. El problema de utilizar tarjetas gráficas para la aceleración es que actúan como coprocesadores, por lo que necesitan de un procesador convencional para funcionar. Intel, siguió una estrategia similar con la primera generación de Intel Xeon Phi, conocida como \textit{Knights Corner} (KNC), pero en la generación actual de estos componentes, el \textit{Knights Landing} puede actuar tanto como procesador como coprocesador, facilitando enormemente el desarrollo del software para este tipo de componentes.

Si atendemos a la lista de los 500 súper-computadores más potentes del mundo, la conocida como TOP500~\cite{top500}, en la edición más reciente de junio de 2017, los primeros puestos están ocupados en su gran mayoría por computadores que usan este tipo de procesadores y aceleradores, y varios de ellos utilizando principalmente el Intel Xeon Phi KNL.