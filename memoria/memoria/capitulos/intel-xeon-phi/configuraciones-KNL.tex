\section{Configuraciones del Intel Xeon Phi KNL} \label{sec:configuraciones-KNL}
Una de las grandes bazas del Intel Xeon Phi, y que aporta una gran flexibilidad a este procesador, es el hecho de que hay diferentes parámetros que se pueden configurar acorde a las necesidades o gustos de los usuarios. En concreto, para este estudio nos interesan los diferentes modos de la memoria MCDRAM y los parámetros de \textit{clustering}.

Es importante mencionar que estas configuraciones se cambian en la BIOS del sistema en tiempo de arranque, por lo que es necesario reiniciar la máquina cada vez que se modifican para que entren en funcionamiento.

\subsection{Modos de memoria}
La memoria de alto ancho de banda que incorpora el Intel Xeon Phi KNL puede ser utilizada de diferentes formas en función de las necesidades del usuario. En concreto son tres las posibles configuraciones aplicables a la memoria MCDRAM, y se describen brevemente en la tabla~\ref{tab:modos-mcdram}.

\input{tablas/intel-xeon-phi/modos-mcdram}

\subsubsection{Modo caché}
En el modo caché, la memoria MCDRAM se comporta como una memoria caché de último nivel (LLC), que en la jerarquía de memoria se situaría entre la memoria caché L2 y la memoria DDR principal. De esta forma, en el momento en que ocurriera un fallo en la caché de primer nivel, se acudiría entonces a la L2, si se repitiese el fallo, se consultaría en este caso a la memoria MCDRAM, que si no contuviese la línea caché deseada acudiría finalmente a la memoria DDR.

Puesto que la memoria MCDRAM es principalmente una memoria más cercana a la memoria principal en lugar de a la CPU, como lo serían las memorias cachés convencionales, su rendimiento en cuanto a latencia y ancho de banda es más próximo al de una memoria DDR que a las memorias L1, L2 y LLC.

La gran ventaja de utilizar este modo en la configuración de la MCDRAM es que está manejada por el sistema, por lo que resulta transparente tanto para el desarrollador como para el software, con lo que no es necesaria ninguna modificación en el código para que este pueda aprovechar las ventajas de esta memoria. 

El uso de la memoria MCDRAM en el modo caché conlleva un aumento de la latencia de la memoria DDR4, pues antes habría que pasar por la MCDRAM. Esto implica que el uso del modo caché incluso podría resultar contraproducente en el caso de que se ejecute un programa en el que se diesen un número muy alto de fallos de caché, y por lo tanto hubiese que acudir a la memoria DDR con frecuencia, debido a que se añade un coste adicional al tener que consultar a la MCDRAM antes de ir a memoria principal en cada fallo de caché.

También se ha de mencionar que los 16 GB de la memoria MCDRAM no serán exclusivamente utilizados para el almacenamiento de datos, sino que la mitad de la memoria (8 GB) tendrán que ser utilizados por los directorios propios de la memoria caché.

\subsubsection{Modo \textit{flat}}
En el modo \textit{flat}, la memoria MCDRAM actuará como parte de la memoria principal y se situará en el mismo espacio de memoria que el resto de memorias DDR. En cuanto al comportamiento de la MCDRAM, este será el mismo que el de la DDR, con la diferencia de que la MCDRAM tendrá un mayor rendimiento que una memoria DDR convencional.

A diferencia del modo caché, en este modo hay que modificar el software para que aproveche el mayor rendimiento de la memoria MCDRAM, pues es reconocida por el sistema como otro nodo NUMA (\textit{Non Uniform Memory Access}). 

Las ventajas de este método son que, una vez que los datos están almacenados en la memoria MCDRAM, el software se beneficiará de un alto ancho de banda de forma estable, pues no hay dependencias de aciertos como en el modo caché y que el programador tiene un mayor control sobre qué se almacena y dónde, por lo que podría optimizar el software para localizar los datos más críticos (en términos de acceso a memoria) en la región de la MCDRAM con el objetivo de disminuir la latencia y aumentar el rendimiento.


\subsubsection{Modo híbrido}
En el modo híbrido, se divide la memoria MCDRAM para que una parte (25\% ó 50\%) actúe en modo caché y la otra (75\% ó 50\%) en modo \textit{flat}. Esta división permite adecuar casi todo tipo de software para la máquina, pues tanto las aplicaciones que hayan sido modificadas para acceder a la memoria MCDRAM como otro nodo NUMA, como las que no se han adecuado al Xeon Phi, seguirán manteniendo un rendimiento adecuado.

Esta configuración también es apropiada para grandes clústers donde los usuarios no pueden escoger qué configuración resulta más beneficiosa, pues con el modo híbrido se satisface, en mayor o menor medida, todas las posibles demandas.

\subsection{Configuraciones de \textit{clustering}} \label{sec:configuraciones-clustering}
Como ya se ha comentado en secciones anteriores, el Intel Xeon Phi KNL contiene un total de 36 \textit{tiles} conectados en una malla bidimensional que utiliza un protocolo MESIF (\textit{Modified}, \textit{Exclusive}, \textit{Shared}, \textit{Invalid}, \textit{Forward}) para mantener la coherencia entre todas las memorias caché de segundo nivel.
Para mantener esta coherencia, en el Intel Xeon Phi KNL se utiliza un directorio de etiquetas distribuido o \textit{Distributed Tag Directory} (DTD), donde cada celda almacena una parte del directorio (TD) con el fin de conocer en qué parte del chip está una determinada línea caché. El directorio se distribuye mediante una función \textit{hash} implementada en hardware, que permite traducir la dirección de memoria a la dirección dentro del directorio y así localizar la línea caché~\cite{colfax-clustering}.

El funcionamiento del DTD es el siguiente:

\begin{enumerate}
	\item Supongamos que una celda $A$ requiere un dato que provoca un fallo en la caché L2 local de la celda.
	
	\item Realiza una consulta al DTD para conocer si la línea caché que está buscando está situada en alguna de las demás cachés del chip. Esta consulta lleva implícita una consulta a la TD de una cierta celda $B$. 
	
	\item Si la TD de la celda $B$ localiza la línea caché en la caché local L2 de una celda $C$, se enviará un mensaje desde la celda $B$ a la celda $C$ solicitando la línea para la celda $A$. También puede suceder que la línea caché deseada no esté en el chip y haya que acceder a memoria principal, por lo que habría que pasar por el controlador de memoria.
	
	\item Por último, la celda $C$ enviará la línea caché solicitada a la celda que lo había reclamado en primer lugar, la celda $A$.
\end{enumerate}

Como resultado de este procedimiento, cuanto mayor sea el directorio más potenciales mensajes se van a cruzar dentro del chip, por lo que aumentará la latencia de los mismos, y mayores serán los tiempos de accesos a los datos en caché. Por ello, Intel proporciona en el Xeon Phi varias formas de ``dividir'' las celdas del chip, con el fin de mantener la localidad de los datos en las cachés del procesador y obtener mejores tiempos en el acceso a los datos, minimizando la congestión de la red.

\subsubsection{\textit{All-to-all}}
Este modelo de afinidad entre las celdas es el más sencillo, donde las direcciones de memoria se reparten de forma uniforme entre todas las TDs del procesador, de manera que no hay ninguna división lógica entre los \textit{tiles}.
%La mayor desventaja de este modo es que puede suceder que los diferentes nodos involucrados estén físicamente lejos entre sí, por lo que las latencias serían elevadas en comparación a situaciones más cercanas y favorables.

Un ejemplo de cómo funciona este modo de \textit{clustering} aparece en la figura~\ref{fig:alltoall-example}. 

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.5\linewidth]{figuras/intel-xeon-phi/XeonPhi_alltoall}
	\caption{Ejemplo de acceso a memoria en el modo \textit{all-to-all}.}
	\label{fig:alltoall-example}
\end{figure}

Aquí la comunicación puede suceder entre celdas de cualquier parte del procesador, por lo que una celda~A puede requerir un dato que necesite la consulta a una celda~B que se encuentre en cualquier lugar del chip, así como que el dato se encuentre en cualquier banco de memoria. De esta forma, la mayoría de transacciones recorrerán distancias mayores, obteniendo latencias también mayores.

Este modo está pensado para ser utilizado como supletorio, pues es el que peor rendimiento debería ofrecer en términos generales. Además, es el único disponible en caso de que la configuración de los DIMMs de la memoria principal no sean todos de igual capacidad, actuando así a ``modo de compatibilidad''. Además, este modo de \textit{clustering} es transparente al software, por lo que no es necesario realizar ninguna modificación en el código para aprovechar este modo.

\subsubsection{\textit{Quadrant}/\textit{Hemisphere}}
En el modo \textit{quadrant}, las celdas del procesador y las direcciones de memoria se dividen en cuatro regiones denominadas cuadrantes, o \textit{quadrants} en inglés, mientras que el modo \textit{hemisphere} funciona de forma similar para dos regiones. De esta forma, las direcciones de memoria se reparten tal que la TD que contiene una dirección de memoria está situada en el mismo cuadrante que la memoria a la que pertenece dicha dirección. 

%Esto permite que los accesos a memoria recorran un camino menor dentro de la malla, traduciéndose en menores tiempos de acceso a memoria.

Un ejemplo de cómo se accedería a memoria en el modo \textit{quadrant} se muestra en la figura~\ref{fig:quadrant-example}, donde una celda~A, al requerir un dato, se va a comunicar con una celda~B que estará en el mismo cuadrante que la memoria a la que se debe acceder, por lo que se recorre un camino menor y se obtiene una latencia menor.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.5\linewidth]{figuras/intel-xeon-phi/XeonPhi_quadrant}
	\caption{Ejemplo de acceso a memoria en el modo \textit{quadrant}. Nótese que al menos dos de los 38 \textit{tiles} del procesador siempre permanecen inactivos.}
	\label{fig:quadrant-example}
\end{figure}

Además, debido a que una transacción de memoria recorre distancias menores y los \textit{buffers} involucrados se van a liberar antes, se permite un número mayor de transacciones por lo que esta configuración ofrece un rendimiento superior respecto del que se puede obtener de \textit{all-to-all}, también siendo transparente al software.

Por último, el único requisito de este modo de \textit{clustering} es que la capacidad de todos los DIMMs DDR4 sea simétrica para que el \textit{hashing} de las direcciones de memoria funcione correctamente.

\subsubsection{SNC-2/SNC-4}
El modo \textit{Sub-NUMA Clustering} o SNC reparte las celdas del chip en dos o cuatro regiones, de forma similar a como se hace en el modo \textit{quadrant}, con la salvedad de que SNC genera por cada región un nuevo nodo NUMA visible por el sistema operativo. Con esta organización, las direcciones de memoria se dividen de modo que cada región del espacio de memoria está ``mapeada'' a un clúster y los TDs para ese espacio de memoria están situados en celdas localizadas dentro de ese mismo clúster.
Un ejemplo de cómo se accedería a memoria en el modo SNC-4 se muestra en la figura~\ref{fig:snc4-example}, donde todas las celdas involucradas en la comunicación deben estar en el mismo nodo NUMA para obtener latencias reducidas.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.5\linewidth]{figuras/intel-xeon-phi/XeonPhi_snc4}
	\caption{Ejemplo de acceso a memoria en el modo SNC-4. Nótese que al menos dos de los 38 \textit{tiles} del procesador siempre permanecen inactivos.}
	\label{fig:snc4-example}
\end{figure}

Debido a que en este modo se generan varios nodos NUMA, el software debe estar adaptado para aprovechar las ventajas de rendimiento que ofrece esta configuración, lo cual representa un esfuerzo mayor que también se traduce en un rendimiento mayor que el del resto de las configuraciones si se aprovecha la localidad de los datos. Este mayor rendimiento viene dado por la cercanía física entre las transacciones que causan un mejor provecho del hardware y los \textit{buffers} que, además, sólo se ven involucrados en transacciones de su propio nodo NUMA.

Este es el modo más recomendado para el software \textit{NUMA-aware}, aunque para programas que no han sido optimizados para esta configuración concreta, el rendimiento se va a ver más pobre en relación a los modos \textit{all-to-all} y \textit{quadrant}.
