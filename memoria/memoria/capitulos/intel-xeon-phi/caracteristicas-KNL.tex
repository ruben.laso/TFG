\section{Características del Intel Xeon Phi KNL} \label{sec:caracteristicas-KNL}
En el año 2016, Intel saca al mercado la segunda generación de Xeon Phi, conocida como \textit{Knights Landing} o KNL. Como se ha comentado anteriormente, la diferencia más reseñable entre el KNL y su antecesor, el KNC (\textit{Knights Corner}), es que el nuevo modelo de Xeon Phi puede actuar tanto como coprocesador (funcionalidad única del KNC), como procesador principal. Esto permite al KNL ser ``independiente'', de forma que puede ser instalado como el único procesador en un servidor.

El Intel Xeon Phi posee tres características principales que lo diferencian del resto de productos del mercado, y que servirán para comprender su funcionamiento.

\subsection{\textit{Tiles}} \label{sec:tiles}
El elemento arquitectónico principal sobre el que se apoya el Intel Xeon Phi es el \textit{tile} o celda\footnote{Una traducción literal del término \textit{tile} sería ``baldosa'', pero parece más adecuado en este contexto traducirlo como ``celda''. Por otro lado, la única bibliografía que existe al respecto está en inglés, por lo que no hay referentes para traducir \textit{tile} de otra forma.}. 
Estas celdas (figura~\ref{fig:xeon-phi-tile-topology}) contienen dos núcleos, cada uno posee dos VPU (\textit{Vector Processing Unit}) y una memoria caché compartida entre los dos núcleos de segundo nivel (L2) de 1 MB. 

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.4\linewidth]{figuras/intel-xeon-phi/phi-tiles/phi-tiles}
	\caption{Topología de un \textit{tile} del Intel Xeon Phi \textit{Knights Landing}.}
	\label{fig:xeon-phi-tile-topology}
\end{figure}

Estas celdas están replicadas físicamente en el procesador 38 veces, aunque sólo 36 de ellas están en funcionamiento, por lo que, en principio, un Intel Xeon Phi cuenta con 72 \textit{cores} y 144 VPUs en cada chip~\cite{intel-Xeon-Phi-KNL-book}, ver figura~\ref{fig:xeon-phi-topology}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.6\linewidth]{figuras/intel-xeon-phi/XeonPhi_topology}
	\caption{Topología del Intel Xeon Phi \textit{Knights Landing}.}
	\label{fig:xeon-phi-topology}
\end{figure}

Las celdas están interconectadas siguiendo una malla bidimensional, la cual es una solución mucho más escalable que el anillo utilizado en el KNC, proporcionando un mayor ancho de banda (de unos 700 GB/s) y una menor latencia que el Xeon Phi de la generación pasada. Esta malla utiliza un protocolo de coherencia caché MESIF, donde una línea caché puede estar marcada como \textit{Modified}, \textit{Exclusive}, \textit{Shared}, \textit{Invalid} o \textit{Forward}, que permite que mantener la coherencia caché entre las memorias L2 de todos los \textit{tiles}.

Además, la comunicación entre diferentes celdas se realiza mediante rutado estático conocido como \textit{YX routing}. De esta forma, cuando dos celdas intercambian información, esta viaja primero verticalmente (en el eje $Y$) hasta llegar a la fila del nodo destino, y a partir de ahí viaja en el eje $X$ hasta llegar al destinatario, ver figura~\ref{fig:xeon-phi-communication-example}.

\begin{figure}[htbp]
	\centering
	%trim recorta la imagen -> trim={<left> <lower> <right> <upper>}
	\includegraphics[width=0.7\linewidth,trim=200 400 300 500,clip] {figuras/intel-xeon-phi/XeonPhi_communication_example}
	\caption{Ejemplo de comunicación entre celdas en el Intel Xeon Phi \textit{Knights Landing}. La celda emisora (celda~1) envía un mensaje a la celda receptora (celda~3). Primero el mensaje viaja verticalmente hasta alcanzar la misma fila que la celda receptora, momento en que llega a la celda~2. Posteriormente, el mensaje seguirá transmitiéndose horizontalmente hasta alcanzar la celda receptora.}
	\label{fig:xeon-phi-communication-example}
\end{figure}

En este tipo de rutado estático se reducen considerablemente los casos de bloqueo, por lo que se consigue una alta eficiencia. Se ha de tener en cuenta que los ``saltos'' entre celdas de un mensaje toma distintos tiempos dependiendo de la dirección, por lo que, en el eje $X$ llevará dos ciclos de reloj, mientras que sólo será necesario un ciclo en la dirección $Y$.


\subsection{\textit{Cores} y VPUs} \label{sec:cores-vpu}
Como ya se ha explicado en la introducción de este capítulo, el Intel Xeon Phi es un procesador que basa su potencia computacional en la cantidad de \textit{cores} que contiene. En concreto, estos núcleos son una evolución de los sencillos Intel Silvermont que incluyen, entre otras, las siguientes mejoras:

\begin{itemize}
	\item Aumento del tamaño del ROB (\textit{Reorder Buffer}) para aumentar el número de instrucciones en ejecución.
	\item Aumento del tamaño de la memoria caché de primer nivel (L1) de datos hasta los 32 KB.
	\item Mayor tamaño de las TLBs (\textit{Translation Lookaside Buffer}).
	\item Operaciones \textit{gather} y \textit{scatter} implementadas en hardware.
	\item Aumento del ancho de banda de la caché L2 para poder cargar una línea caché por ciclo y escribir una línea cada dos ciclos.
	\item \textit{Hyperthreading} de hasta cuatro hilos por \textit{core}.
	\item Añadido el soporte para los conjuntos de instrucciones vectoriales, como AVX, AVX-2 y AVX-512.
	\item Añadidas dos VPU (\textit{Vector Processing Unit}) a cada \textit{core}.
\end{itemize}

De toda esta lista de mejoras, las más interesantes son, sin duda alguna, las tres últimas, y son las que permiten al Intel Xeon Phi ofrecer el gran rendimiento del que puede presumir.

\subsubsection{\textit{Hyperthreading}}
La conocida como \textit{hyperthreading} es una tecnología que permite ejecutar varios hilos simultáneamente dentro de un mismo núcleo físico. Esto permite utilizar de forma más eficiente los recursos del \textit{core}, aumentando significativamente su rendimiento sin apenas afectar a su consumo. Como es lógico, las ventajas de esta tecnología se notan especialmente en las aplicaciones que utilizan varios hilos para su ejecución~\cite{intel-Hyperthreading}.

En el Intel Xeon Phi KNL cada \textit{core} puede ejecutar hasta cuatro hilos de forma simultánea, por lo que en el conjunto del chip, se pueden lanzar hasta $72 \times 4 = 288$ hilos. Esta cifra es más que notable si tenemos en cuenta que en un procesador multicore destinado a servidores más modernos se pueden lanzar hasta 48 hilos  de forma simultánea \cite{intel-E7}.

\subsubsection{Instrucciones AVX-512}
A pesar de que el Intel Xeon Phi KNC ya soportaba instrucciones con vectores de 512 bits, en el \textit{Knights Landing} se incorpora un nuevo conjunto de instrucciones conocidas como Intel \textit{Advanced Vector Extensions} 512 (AVX-512) \cite{colfax-avx512}.

Teniendo en cuenta que un número en punto flotante de doble precisión tiene un tamaño de 8 bytes (64 bits), llegamos a la conclusión de que en un vector de 512 bits caben $512 \div 64 = 8$ \textit{doubles}. Si atendemos a la figura~\ref{fig:vector-vs-scalar-instrctions.}, con este tipo de instrucciones vectoriales se podría operar con varios números en punto flotante de forma paralela (figura~\ref{fig:vector-instructions}), en lugar de operar uno a uno tal y como se haría tradicionalmente (figura~\ref{fig:scalar-instructions}).

\begin{figure}[htbp]
	\centering
	\begin{subfigure}[b]{.5\textwidth}
		\centering
		\includegraphics[width=.5\linewidth]{figuras/intel-xeon-phi/scalar-instructions/scalar-instructions}
		\caption{Instrucciones escalares o lineales}
		\label{fig:scalar-instructions}
	\end{subfigure}%
	\begin{subfigure}[b]{.5\textwidth}
		\centering
		\includegraphics[width=.5\linewidth]{figuras/intel-xeon-phi/vector-instructions/vector-instructions}
		\caption{Instrucciones vectoriales}
		\label{fig:vector-instructions}
	\end{subfigure}
	\caption{Representación gráfica de las instrucciones escalares y las vectoriales.}
	\label{fig:vector-vs-scalar-instrctions.}
\end{figure}

Si comparamos los dos procedimientos, mientras que el procesador de la figura~\ref{fig:scalar-instructions} tiene que ejecutar una instrucción de carga, una de suma y una de almacenamiento por cada operación, el procesador de la figura~\ref{fig:vector-instructions} ejecuta una instrucción de carga, una de suma y una de almacenamiento para varios elementos simultáneamente.

Para utilizar este tipo de instrucciones existen varias alternativas. Por un lado, podemos usar las funciones del lenguaje de programación asociadas a dichas instrucciones (la alternativa difícil), o podemos confiar en la optimización del compilador, en este caso de GNU, la cual es la alternativa más sencilla, para la que está pensado este conjunto de instrucciones y la que mejores resultados suele dar debido al gran desempeño de los compiladores actuales.

\subsubsection{VPU (\textit{Vector Procesing Units})}
Para trabajar con el nuevo conjunto de instrucciones AVX-512, el Xeon Phi incluye, en cada uno de sus \textit{cores}, dos unidades de procesamiento vectorial o VPUs. Estas unidades son las encargadas de ejecutar todas las instrucciones tanto de operaciones vectoriales como de aquellas con operadores en punto flotante.

Las dos VPUs conectadas a cada núcleo contienen en esencia las mismas características, aunque una de ellas está ligeramente extendida para proporcionar compatibilidad con conjuntos de instrucciones antiguas. Cada VPU es capaz de efectuar una operación de multiplicación-suma en cada ciclo de reloj. Esto permite al Intel Xeon Phi alcanzar un rendimiento pico teórico de más de 3 TFLOPS (Tera \textit{Floating Point Operations Per Second}).
Esta cifra puede obtenerse mediante la expresión~\ref{eq:formula-flops}, donde el rendimiento $R$ viene dado por el número de \textit{cores} o unidades de procesamiento disponibles, $n$, y la cantidad de operaciones que se pueden efectuar en cada ciclo de reloj en cada unidad de procesamiento de forma paralela $p$, multiplicado por el número de ciclos de reloj que se producen en un segundo, la frecuencia $f$ del procesador:

\begin{equation}\label{eq:formula-flops}
	R = npf
\end{equation}

Conociendo que el mejor modelo de Intel Xeon Phi KNL trabaja a una frecuencia de 1.5 GHz, cuenta con $72 \times 2 = 144$ VPUs que operan sobre 8 números en punto flotante de doble precisión, realizando 2 operaciones (una multiplicación y una suma) en cada ciclo, en la expresión~\ref{eq:phi-flops} obtenemos que:

\begin{equation}\label{eq:phi-flops}
	R = npf = 144 \times 8 \times 2 \times 1.5 \times 10^{9} = 3,456 \times 10^{9} = 3.4 \times 10^{12}
\end{equation}

Por lo que el Intel Xeon Phi tiene un rendimiento pico teórico de 3.4 TFLOPS. Realmente esta cifra es difícil de alcanzar en la realidad, pues siempre existen dependencias de datos, accesos a memoria, operaciones de almacenamiento y carga de datos, etc. que empeoran los resultados teóricos.

\subsection{MCDRAM} \label{sec:mcdram}
Habitualmente la carga aritmético-lógica de un programa no es el elemento limitante ni supone el cuello de botella del sistema, sino que lo son los accesos a memoria y el ancho de banda disponible. La mayoría de software realiza muy pocas operaciones aritméticas sobre cada uno de los datos que maneja, sin embargo, suelen tener una gran cantidad de accesos a memoria. En este tipo de programas se sitúan las operaciones matriciales o las transformadas de Fourier \cite{colfax-mcdram}.

El hecho de que la carga aritmética suponga un menor tiempo de computación que las operaciones de memoria implica que las capacidades aritméticas de un procesador se vean ensombrecidas debido a la ``lentitud'' de las memorias. Para solventar este problema, Intel, en el Xeon Phi KNL, incorpora una memoria de alto ancho de banda y baja latencia conocida como \textit{Multi-Channel Dynamic Random Access Memory} o MCDRAM. Esta memoria es capaz de ofrecer un ancho de banda muy superior al de una memoria DDR4 convencional, obteniendo más de 400 GB/s en la MCDRAM por los, aproximadamente, 90 GB/s de la DDR4. La MCDRAM que incorpora el Xeon Phi tiene un tamaño de 16 GB y, al estar integrada en el chip, no puede ser retirada ni reemplazada.

El objetivo de esta memoria MCDRAM es el de colaborar junto a la DDR tradicional. Esto puede conseguirse de varias formas, tal y como se muestra en la figura~\ref{fig:cache-vs-flat-mcdram}, bien sirviendo como memoria caché de último nivel (figura~\ref{fig:mcdram-cache}) en el modo caché, bien como  una segunda memoria de mayor rendimiento (figura~\ref{fig:mcdram-flat}) en el modo \textit{flat}, o incluso como combinación de ambas en el modo híbrido, en cuyo caso un tanto por ciento de la memoria se comportará en modo caché y el resto en modo \textit{flat}. Estas configuraciones se mostrarán con más detalle en la sección~\ref{sec:configuraciones-KNL} (pág. \pageref{sec:configuraciones-KNL}).

\begin{figure}[htbp]
	\centering
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{figuras/intel-xeon-phi/mcdram-cache/mcdram-cache}
		\caption{MCDRAM como memoria caché de L3.}
		\label{fig:mcdram-cache}
	\end{subfigure}
\qquad
	\begin{subfigure}[t]{.45\textwidth}
		\centering
		\includegraphics[width=\linewidth]{figuras/intel-xeon-phi/mcdram-flat/mcdram-flat}
		\caption{MCDRAM como otra memoria principal.}
		\label{fig:mcdram-flat}
	\end{subfigure}
	
	\caption{Esquemas básicos de funcionamiento de la memoria MCDRAM.}
	\label{fig:cache-vs-flat-mcdram}
\end{figure}


El objetivo del programador debe ser involucrar todos los datos que sean críticos en el tiempo de ejecución en la memoria MCDRAM, bien emplazando explícitamente los datos dentro de esta o utilizándola en modo caché.