\subsection{Anidamiento de los lazos} \label{sec:anidamiento-lazos}
La variación en el orden en que se anidan los lazos del algoritmo del producto de matrices influye de forma determinante en el patrón de acceso a memoria durante la ejecución del programa, por este motivo es de especial interés para el estudio comprobar qué sucede ante diferentes patrones.

Para reflejar cómo pueden influir el orden de los lazos en nuestro programa, consideremos una matriz de orden 4 representada en la figura~\ref{fig:matriz}, donde cada una de las filas de la matriz coincide con una línea caché, y que el tamaño de página corresponde con dos filas de la matriz.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.4\linewidth]{figuras/diseno-benchmarks/matriz/matriz}
	\caption{Representación de una matriz de orden 4.}
	\label{fig:matriz}
\end{figure}

Para la multiplicación de matrices necesitamos tres de estas estructuras, dos para multiplicar y una para almacenar el resultado, pero para ilustrar el efecto de los patrones de acceso a memoria basta con una única matriz, aquella en la que guardamos el resultado, donde los accesos son más relevantes por ser de escritura de datos.

Según el pseudocódigo del algoritmo del producto matricial del código~\ref{lst:pseudocodigo-producto-matrices} (pág.~\pageref{lst:pseudocodigo-producto-matrices}), los lazos que afectan a la matriz resultado son los que tienen los índices $i$ y $j$, mientras que $k$ sólo afecta a las matrices que se multiplican. Analicemos ahora el impacto que tiene el anidamiento de los bucles en los accesos a los datos.

Si como lazo más interno situamos aquel que tiene índice $k$, el acceso a la matriz resultado es el mejor que podíamos esperar, pues durante todas las iteraciones de dicho bucle se está accediendo al mismo elemento de la matriz resultado, el cual estará en la memoria caché y su acceso es el más rápido posible.

Por otro lado, si utilizamos como bucle interno el de índice $j$, accederemos a los elementos de la matriz resultado secuencialmente por filas, es decir, primero accederemos al elemento $a_{11}$, después al $a_{12}$, y así sucesivamente tal y como se muestra en la figura~\ref{fig:matriz2}.
Esta forma de direccionamiento no es del todo desfavorable pues, como en C los elementos contiguos se encuentran en posiciones contiguas de la memoria caché, sólo tendremos que cargar una nueva línea caché cada $n$ elementos, siendo $n$ el número de columnas de la matriz. De hecho, si el \textit{prefetching}\footnote{El \textit{prefetching} es una característica por la cual el procesador, en cada acceso a memoria, en lugar de traer una única línea caché, trae varias líneas intentando adelantarse a las futuras demandas del programa para así mejorar el rendimiento.} realiza un buen trabajo este número se verá notablemente reducido. Lo mismo sucedería con los fallos de página del sistema operativo, pues se accedería a un notable número de elementos de la matriz antes de que sucediese uno.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.45\linewidth]{figuras/diseno-benchmarks/matriz/matriz2}
	\caption{Representación del orden de acceso por filas a los datos de la matriz.}
	\label{fig:matriz2}
\end{figure}

Si, por último, situamos como bucle interno aquel con el índice $i$, accederemos a los elementos secuencialmente por columnas, de forma que primero accederíamos al elemento $a_{11}$, después al $a_{21}$ y así sucesivamente. Esta forma de acceso se muestra gráficamente en la figura~\ref{fig:matriz3}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.5\linewidth]{figuras/diseno-benchmarks/matriz/matriz3}
	\caption{Representación del orden de acceso por columnas a los datos de la matriz.}
%	en el caso de que el bucle interno sea el correspondiente al índice $i$.}
	\label{fig:matriz3}
\end{figure}


Este patrón de acceso sería el más desfavorable en lo que a la matriz resultado se refiere, pues en ningún momento se aprovecha la localidad de los datos en memoria caché, se saca muy poco provecho del \textit{prefetching} y los fallos de página van a ser frecuentes.

Se ha de apuntar que no sólo hay que tener en cuenta al acceso a los elementos que se hacen en la matriz resultado, pues en el algoritmo influyen otras dos matrices que sufren los mismos problemas que acabamos de  citar. Es evidente pues, que la mejor configuración es la que sabe balancear las virtudes y los defectos de cada combinación.

\subsubsection{Anidamiento $ijk$}
Con el anidamiento clásico, el $ijk$ (ver código~\ref{lst:pseudocodigo-producto-matrices}, pág.~\pageref{lst:pseudocodigo-producto-matrices}), y accediendo a los elementos de la forma \texttt{C[i][j] += A[i][k] * B[k][j]}, se leerían/modificarían de una forma eficiente los elementos de \texttt{A} y de \texttt{C}, mientras que los accesos a elementos de \texttt{B} serían claramente mejorables. 

En \texttt{A} y en \texttt{C} accederemos a todos los elementos de una fila antes de cambiar a otra, tal y como se muestra en la figura~\ref{fig:matriz2}, por lo que aprovechamos la localidad de los datos dentro de la memoria, y por ende, en la caché. En \texttt{A} sólo cambiaríamos de fila una vez que se ha recorrido $n$ veces la misma, $n^2$ operaciones después, mientras que en \texttt{C} accedemos al mismo elemento durante las $n$ iteraciones de $k$, para luego cambiar al siguiente elemento de la fila, por lo cambiamos de fila cada $n^2$ operaciones.

Sin embargo, en la matriz \texttt{B}, para cada valor de $j$ se suceden $n$ valores de $k$, por lo que se acceden a $n$ filas distintas de las cuales sólo aprovecharemos un único elemento (ver figura~\ref{fig:matriz3}), desperdiciando así la localidad de los datos en memoria y en caché.


\subsubsection{Anidamiento $ikj$}
Utilizando el orden $ikj$ de los lazos (ver código~\ref{lst:pseudocodigo-producto-matrices-ikj}, pág.~\pageref{lst:pseudocodigo-producto-matrices-ikj}) obtendremos el mejor acceso posible a las tres matrices involucradas dentro del producto.

\lstinputlisting[caption=Pseudocódigo para el cálculo del producto de matrices utilizando el anidamiento $ikj$., label=lst:pseudocodigo-producto-matrices-ikj]{codigo/matrix-multiplication-pseudocode-ikj.c}

Dado que el índice que cambia su valor más frecuentemente es $j$ y este sólo se utiliza para denotar elementos dentro de una misma fila, en las matrices \texttt{B} y \texttt{C} realizaremos un recorrido por filas de sus elementos. El segundo índice que más varía es $k$, por lo que también recorreremos \texttt{A} por filas. De esta forma explotaremos al máximo la localidad de los datos dentro de la memoria principal y de la memoria caché, pues de cada línea caché que es traída desde la memoria se leerán todos los elementos que contiene y haremos un buen uso del \textit{prefetching} al acceder a líneas cachés situadas contiguamente.


\subsubsection{Anidamiento $jki$}
Por último, el anidamiento $jki$ (código~\ref{lst:pseudocodigo-producto-matrices-jki}, pág.~\pageref{lst:pseudocodigo-producto-matrices-jki}) es aquel que presenta la situación más desfavorable, pues a las tres matrices se accede por columnas.

\lstinputlisting[caption=Pseudocódigo para el cálculo del producto de matrices utilizando el anidamiento $jki$., label=lst:pseudocodigo-producto-matrices-jki]{codigo/matrix-multiplication-pseudocode-jki.c}

En este caso, el índice que más varía su valor es $i$, el cual sirve para indicar a qué fila debemos acceder en dos de las tres matrices, en \texttt{A} y en \texttt{C}. Después de $i$, el índice que más cambia es $k$, el cual sirve para denotar la fila en la matriz \texttt{B}. Dado que en nuestros \textit{benchmarks} manejaremos matrices cuyo orden supera ampliamente (del orden de miles de veces) el tamaño de una línea caché, parece una cuestión utópica el pensar que el \textit{prefetching} va a ser lo suficientemente bueno como para deducir a qué línea caché accederemos a continuación. Si a esto añadimos que sólo vamos a acceder a un elemento por línea, lo único que cabe esperar es que los tiempos de ejecución aumenten considerablemente.

Por último, también es necesario recordar que lo que nos interesa en este estudio no es saber cuál es la mejor combinación de lazos para este problema, y de hecho, los programas que mejor rendimiento ofrezcan son los de menos interés para la investigación. Los programas con un rendimiento inferior serán los que más veces tengan que acceder a memoria y en los que mejor se puede perfilar un modelo del comportamiento del Intel Xeon Phi KNL cuando tiene que realizar accesos a la memoria principal.
