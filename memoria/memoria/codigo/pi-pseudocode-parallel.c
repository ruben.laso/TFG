#pragma omp parallel for reduction(+: pi) private (k, _8k) num_threads(number_of_threads)
for (k = 0; k < max_k; k++) {
    _8k = 8 * k;
    pi += 1.0/pow(16,k) * (4.0/(_8k + 1) - 2.0/(_8k + 4) - 1.0/(_8k + 5) - 1.0/(_8k + 6));
}
