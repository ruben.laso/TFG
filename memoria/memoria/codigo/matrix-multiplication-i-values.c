A [rows * columns];
B [rows * columns];
C [rows * columns];

for (int i = 0; i < rows; i++) {
    for (int j = 0; j < columns; j = j+1) {
        for (int k = 0; k < columns; k = k+1) {
            (*(C + i*columns + j)) += (*(A + i*columns)) * (*(B + k*columns + j));
        }
    }
}
