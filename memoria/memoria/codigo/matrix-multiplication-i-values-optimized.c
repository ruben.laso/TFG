A [rows * columns];
B [rows * columns];
C [rows * columns];

i_max_value = rows * columns;

for (int i = 0; i < i_max_value; i+= columns) {
    for (int j = 0; j < columns; j = j+1) {
        for (int k = 0; k < columns; k = k+1) {
            (*(C + i + j)) += (*(A + i)) * (*(B + k*columns + j));
        }
    }
}
